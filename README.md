# Lunar Logistics

This is a simple work-in-progress car game which I am developing. My 3D graphics
utility library ([awe](https://gitlab.com/wilsonco-moo/awe)) is used for
the heightmap, and to load resources. Physics is provided by the
[Bullet Physics](https://github.com/bulletphysics/bullet3) library.

This is intended to (eventually) be a game about driving a car on the moon -
(that is why the terrain shown below has craters in it). The player will
complete deliveries and other objectives, while trying not to damage what
they're carrying.


## Acknowledgements

 * **Andrew Ackerley**: For game concepts, terrain design and 3D modelling.


## Compiling and testing

This project (should) have all the same dependencies as the
[Rasc project](https://gitlab.com/wilsonco-moo/rasc). See
*"Prerequisites for compiling Rasc"* in the Rasc readme file.

Additionally, this game requires [OpenAL](http://www.openal.org/) for audio.
In Debian/Ubuntu the OpenAL library and header files are provided by the
`libopenal-dev` package.

Assuming all prerequisites are met, to compile and run lunar logistics
(**in Linux**), do the following:
 * Clone the repository onto your local filesystem.
 * Run `./waf configure` from within the project - this will set up all
   libraries which lunar logistics depends on.
 * Run `./waf build` - this will compile the project (in development mode).
 * Run `./debugLaunch` to run the project (in development mode).

**Controls**

 * `ctrl`: Toggle mouse lock, for looking around.
 * `w`, `a`, `s`, `d`: Move car around.
 * `q`, `e`: Change gears down and up respectively.
 * `+`/`=`, `_`/`-`: Zoom camera in and out.


## Screenshots (work-in-progress)

Below: The game now simulates the torque curve of the engine, providing
realistic torque outputs across the engine's RPM range. This is passed through
a gearbox, where the player can select which gear ratio to use. The game's
[rascUI](https://gitlab.com/wilsonco-moo/rascui) based user interface now also
displays the current gear and engine RPM.
![Image](https://gitlab.com/wilsonco-moo/lunarlogistics/-/wikis/screenshots/gears.png)

Below: More moon-like textures, consisting of sharp gravel have now been added.
The display of these is made more convincing by the parallax mapping.
![Image](https://gitlab.com/wilsonco-moo/lunarlogistics/-/wikis/screenshots/moonTextures.png)

Below: The terrain now supports
[Parallax mapping](https://en.wikipedia.org/wiki/Parallax_mapping) (using
Ray-traced occlusion). This greatly improves the terrain, allowing it to show
physically lumpy textures. The game also now has a simple
[rascUI](https://gitlab.com/wilsonco-moo/rascui) based user interface, providing
graphics quality options, ability to reset the car's position, and a speed
display.
![Image](https://gitlab.com/wilsonco-moo/lunarlogistics/-/wikis/screenshots/parallaxOcclusion.png)

Below: Ground scatter (such as small rocks and pebbles) can now be added to the
terrain. The placement of this is controlled by a density map, to control which
areas are sparsely populated with pebbles, and which areas are densely populated
with pebbles.
![Image](https://gitlab.com/wilsonco-moo/lunarlogistics/-/wikis/screenshots/groundScatter.png)


Below: the terrain is able to use many textures, each texture has an
associated normal map.
![Image](https://gitlab.com/wilsonco-moo/lunarlogistics/-/wikis/screenshots/mountainSide.png)
![Image](https://gitlab.com/wilsonco-moo/lunarlogistics/-/wikis/screenshots/craterSide.png)
