#!/usr/bin/env python

import bpy
import os

outputFile = open("output.xml", "w")

# Iterate asset parent objects
for parent in bpy.context.scene.objects:
    if "asset" in parent:
        print("Found asset parent object: "+parent.name)
        outputFile.write("<asset name=\""+parent["asset"]+"\">\n")
        
        # Iterate children of this asset
        for child in parent.children:
            print("    Found child instance: "+child.name)
            
            # Print the instance, along with its transformation matrix (converted to hex)
            outputFile.write("    <instance name=\""+child.name+"\" baseTransform=\"")
            for iy in range(4):
                for ix in range(4):
                    outputFile.write(float(child.matrix_world[iy][ix]).hex())
                    if iy != 3 or ix != 3:
                        outputFile.write(" ")
            outputFile.write("\"/>\n")
            
        outputFile.write("</asset>\n\n")
            
outputFile.close()
