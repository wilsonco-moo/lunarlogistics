import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import javax.imageio.ImageIO;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.io.FileReader;  
import java.util.Scanner;
import java.util.TreeMap;
import java.util.List;
import java.awt.Color;
import java.io.File;

class HeightmapConv {
    
    public static BufferedImage hFlipImage(BufferedImage img) {
        int width = img.getWidth(),
            height = img.getHeight();
        BufferedImage ret = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);
        
        for (int iy = 0; iy < height; iy++) {
            for (int ix = 0; ix < width; ix++) {
                ret.setRGB(width - ix - 1, iy, img.getRGB(ix, iy));
            }
        }
        
        return ret;
    }
    
    public static void main(String[] args) throws Exception {
        
        // Get heightmap size from user.
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter heightmap width:");
        int width = scanner.nextInt();
        System.out.println("Enter heightmap height:");
        int height = scanner.nextInt(),
            area = width * height;
        
        System.out.println("Reading and sorting obj file...");
        
        // Start with -2 to skip first 2 lines. Build a tree map of tree maps
        // to sort the data in the obj file (exporters put the data in fairly random orders).
        int upto = -2;
        BufferedReader reader = new BufferedReader(new FileReader(new File("heightmapIn.obj")));
        TreeMap<Double, TreeMap<Double, Double>> orderedHeights = new TreeMap<Double, TreeMap<Double, Double>>();
        String line;
        while ((line = reader.readLine()) != null && upto < area) {
            if (upto >= 0) {
                // Get x, y, height, record it in tree map.
                String[] split = line.split("  *");
                double x = Double.parseDouble(split[1]),
                       y = Double.parseDouble(split[2]),
                       heightAtPoint = Double.parseDouble(split[3]);
                
                // Create row if it doesn't already exist.
                TreeMap<Double, Double> row;
                if (orderedHeights.containsKey(y)) {
                    row = orderedHeights.get(y);
                } else {
                    row = new TreeMap<Double, Double>();
                    orderedHeights.put(y, row);
                }
                row.put(x, heightAtPoint);
            }
            upto++;
        }
        reader.close();
        
        // Complain and give up if there are the wrong number of rows.
        if (orderedHeights.size() != height) {
            System.err.println("ERROR: Found " + orderedHeights.size() + " rows in model, instead of expected: " + height + ".\n");
            System.exit(1);
        }
        
        System.out.println("Compacting height data...");
        
        // Iterate (in order) through each height in each row. Record min and max
        // heights, and store in heightmap data array.
        double minHeight = Double.MAX_VALUE, maxHeight = Double.MIN_VALUE;
        double[] heightmapData = new double[area];
        upto = 0;
        for (TreeMap<Double, Double> row : orderedHeights.values()) {
            
            // Complain and give up if there are the wrong number of columns.
            if (row.size() != width) {
                System.err.println("ERROR: Found " + row.size() + " columns in model row, instead of expected: " + width + ".\n");
                System.exit(1);
            }
            
            for (Double heightAtPoint : row.values()) {
                heightmapData[upto] = heightAtPoint;
                minHeight = Math.min(minHeight, heightAtPoint);
                maxHeight = Math.max(maxHeight, heightAtPoint);
                upto++;
            }
        }
        double heightRange = maxHeight - minHeight;
        
        // Convert all to 0-65535 (inclusive).
        System.out.println("Converting heights...");
        int[] intData = new int[area];
        for (int i = 0; i < area; i++) {
            intData[i] = Math.min(65535, Math.max(0, (int)(((heightmapData[i] - minHeight) / heightRange) * 65535.0d)));
        }
        
        // Build the output image.
        System.out.println("Converting to image...");
        BufferedImage img = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);
        upto = 0;
        for (int iy = 0; iy < height; iy++) {
            for (int ix = 0; ix < width; ix++) {
                int heightAt = intData[upto++];
                int col = 0xff000000 | (heightAt << 8);
                img.setRGB(ix, iy, col);
            }
        }
        
        // Horizontally flip the output image (take care of left/right handedness of coordinates).
        img = hFlipImage(img);
        
        // Save the output image.
        System.out.println("Saving output image...");
        ImageIO.write(img, "png", new File("heightmapOut.png"));
    }
}

