import java.nio.charset.StandardCharsets;
import java.awt.image.BufferedImage;
import javax.imageio.ImageIO;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.awt.Color;
import java.io.File;

class ColourDef {
    public Color colour;
    public int outputValue;
    public int id;
    public float blurryness;
    public static ColourDef[] defs;
    public static boolean foundWrongColours = false;
    public ColourDef(int id, float blurryness, int outputValue, Color colour) {
        this.id = id;
        this.blurryness = blurryness;
        this.outputValue = outputValue;
        this.colour = colour;
    }
    public static void genColourDefs() throws Exception {
        List<String> lines = Files.readAllLines(Paths.get("colourDefs"), StandardCharsets.UTF_8);
        List<ColourDef> cDefs = new ArrayList<ColourDef>();
        int i = 0;
        for (String line : lines) {
            String[] strArr = line.split("  *");
            if (i == 0) {
                // Read base blur radius (it is the first line).
                Pix.BASE_BLUR_RAD = Integer.parseInt(strArr[1]);
                
            } else if (i >= 2) {
                cDefs.add(new ColourDef(i - 2,
                    Float.parseFloat(strArr[1]), // blurryness
                    Integer.parseInt(strArr[2]), // outputValue
                    new Color(
                        Integer.parseInt(strArr[3]),
                        Integer.parseInt(strArr[4]),
                        Integer.parseInt(strArr[5])
                    )
                ));
            }
            i++;
        }
        defs = new ColourDef[cDefs.size()];
        cDefs.toArray(defs);
    }
    public static ColourDef find(int x, int y, Color colour) {
        if (colour.getAlpha() != 255) {
            System.err.println("ERROR: Colour map: Pixel ("+x+","+y+") contains transparency.");
            foundWrongColours = true;
            return null;
        }
        for (ColourDef def : defs) {
            if (def.colour.equals(colour)) {
                return def;
            }
        }
        System.err.println("ERROR: Colour map: Pixel ("+x+","+y+") is unrecognised colour.");
        foundWrongColours = true;
        return null;
    }
}

class WeightedColPair {
    public ColourDef colDef1, colDef2;
    public int weight1, weight2;
    public WeightedColPair(ColourDef colDef1, int weight1, ColourDef colDef2, int weight2) {
        if (colDef1.id < colDef2.id) {            
            this.colDef1 = colDef1;
            this.colDef2 = colDef2;
            this.weight1 = weight1;
            this.weight2 = weight2;
        } else {
            this.colDef1 = colDef2;
            this.colDef2 = colDef1;
            this.weight1 = weight2;
            this.weight2 = weight1;
        }
    }
    public WeightedColPair() {}
    public int getBlurinessRadius() {
        return (int)(Math.ceil((colDef1.blurryness * colDef2.blurryness) * ((float)Pix.BASE_BLUR_RAD)));
    }
    public Color getFinalColour() {
        // 0 is col1, 255 is col2.
        int mix = 255 - Math.round((((float)weight1) / ((float)(weight1 + weight2))) * 255.0f);
        // height, mix, terrain1, terrain2.
        return new Color(mix, colDef1.outputValue, colDef2.outputValue);
    }
    // Returns true if one of our weights is zero.
    public boolean isOneSided() {
        return weight1 == 0 || weight2 == 0;
    }
    // Returns the colour assuming one of our weights is zero.
    public ColourDef getSingleColour() { if (weight1 > 0) return colDef1; else return colDef2; }
}

class Pix {
    public ColourDef colour;
    public int x, y;
    public static int BASE_BLUR_RAD = -1;
    private WeightedColPair pair, normalisedPair;
    public Pix(int x, int y, Color colourCol) {
        colour = ColourDef.find(x, y, colourCol);
        this.x = x; this.y = y;
        pair = null;
        normalisedPair = null;
    }
    public static int readHeight(int x, int y, Color colour) {
        int val = colour.getRed();
        if (colour.getGreen() != val || colour.getBlue() != val) {
            System.err.println("ERROR: Height map: Pixel ("+x+","+y+") not grey.");
            ColourDef.foundWrongColours = true;
        }
        if (colour.getAlpha() != 255) {
            System.err.println("ERROR: Height map: Pixel ("+x+","+y+") contains transparency.");
            ColourDef.foundWrongColours = true;
        }
        return val;
    }
    // Generates our internal colour pair.
    public void generateColourPair(Pix[][] pixels, int pixImageWidth, int pixImageHeight) {
        // Work out the pair of colours, based on blurriness around surrounding colours.
        int radius = BASE_BLUR_RAD;
        while(radius >= 1) {
            pair = getMostCommonPair(pixels, pixImageWidth, pixImageHeight, radius);
            if (pair != null && pair.getBlurinessRadius() >= radius) {
                break;
            }
            radius--;
        }
    }
    
    // Looks at one-sided single colour pixels.
    // Finds surrounding pixels with at least one colour difference. Swaps if necessary,
    // and sets our zero colour to match theirs.
    public void firstLevelNormalise(Pix[][] pixels, int pixImageWidth, int pixImageHeight) {
        if (!pair.isOneSided() || pair.colDef1 != pair.colDef2) {
            normalisedPair = pair;
            return;
        }
        
        ColourDef ourSingleColour = pair.getSingleColour();
        int maxWeight = 0;
        
        // Look at ALL adjacent.
        for (int dy = -1; dy <= 1; dy++) {
            for (int dx = -1; dx <= 1; dx++) {
                int newX = x + dx, newY = y + dy;
                if (newX >= 0 && newY >= 0 && newX < pixImageWidth && newY < pixImageHeight && dx != 0 && dy != 0) {
                    Pix other = pixels[newX][newY];
                    // Found and adjacent pixel with at least one colour different.
                    if (other.pair.colDef1 != pair.colDef1 || other.pair.colDef2 != pair.colDef2) {
                        
                        normalisedPair = new WeightedColPair();
                        normalisedPair.colDef1 = pair.colDef1;
                        normalisedPair.colDef2 = pair.colDef2;
                        
                        // Swap if needed.
                        if ((pair.weight1 > 0 && pair.colDef1 == other.pair.colDef1) ||
                            (pair.weight2 > 0 && pair.colDef2 == other.pair.colDef2)) {
                            // Colours same way round - no change needed - no swap.
                            normalisedPair.weight1 = pair.weight1;
                            normalisedPair.weight2 = pair.weight2;
                        } else {
                            // Colours opposite way round - swap.
                            normalisedPair.weight1 = pair.weight2;
                            normalisedPair.weight2 = pair.weight1;
                        }
                        
                        // Set zero colour.
                        if (normalisedPair.weight1 == 0) {
                            normalisedPair.colDef1 = other.pair.colDef1;
                        } else if (normalisedPair.weight2 == 0) {
                            normalisedPair.colDef2 = other.pair.colDef2;
                        }
                        
                        return;
                    }
                }
            }
        }
        
        // If found nothing don't change pair.
        normalisedPair = pair;
    }
    
    // Looks at one-sided single colour pixels.
    // Finds surrounding pixels with at least one colour difference. Swaps if necessary,
    // BUT DOES NOT SET OUR ZERO COLOUR.
    public void secondLevelNormalise(Pix[][] pixels, int pixImageWidth, int pixImageHeight) {
        if (!pair.isOneSided() || pair.colDef1 != pair.colDef2) {
            normalisedPair = pair;
            return;
        }
        
        ColourDef ourSingleColour = pair.getSingleColour();
        int maxWeight = 0;
        
        // Look at ALL adjacent.
        for (int dy = -1; dy <= 1; dy++) {
            for (int dx = -1; dx <= 1; dx++) {
                int newX = x + dx, newY = y + dy;
                if (newX >= 0 && newY >= 0 && newX < pixImageWidth && newY < pixImageHeight && dx != 0 && dy != 0) {
                    Pix other = pixels[newX][newY];
                    // Found and adjacent pixel with at least one colour different.
                    if (other.pair.colDef1 != pair.colDef1 || other.pair.colDef2 != pair.colDef2) {
                        
                        normalisedPair = new WeightedColPair();
                        normalisedPair.colDef1 = pair.colDef1;
                        normalisedPair.colDef2 = pair.colDef2;
                        
                        // Swap if needed.
                        if ((pair.weight1 > 0 && pair.colDef1 == other.pair.colDef1) ||
                            (pair.weight2 > 0 && pair.colDef2 == other.pair.colDef2)) {
                            // Colours same way round - no change needed - no swap.
                            normalisedPair.weight1 = pair.weight1;
                            normalisedPair.weight2 = pair.weight2;
                        } else {
                            // Colours opposite way round - swap.
                            normalisedPair.weight1 = pair.weight2;
                            normalisedPair.weight2 = pair.weight1;
                        }
                        // Don't set a zero colour, or a seam happens.
                        return;
                    }
                }
            }
        }
        
        // If found nothing don't change pair.
        normalisedPair = pair;
    }
        
    // Sets our pair to our normalised pair.
    public void applyNormalised() {
        pair = normalisedPair;
        normalisedPair = null;
    }
    // Gets our final output.
    public Color getOutput() {
        return pair.getFinalColour();
    }
    // Finds the most common pair of colours nearby. radius must be 1 or bigger.
    private WeightedColPair getMostCommonPair(Pix[][] pixels, int pixImageWidth, int pixImageHeight, int radius) {
        
        // Then do the real count, actually within the radius area.
        int[] counts = new int[ColourDef.defs.length];
        for (int dy = -radius; dy <= radius; dy++) {
            for (int dx = -radius; dx <= radius; dx++) {
                int newX = x + dx, newY = y + dy;
                if (newX >= 0 && newY >= 0 && newX < pixImageWidth && newY < pixImageHeight &&
                    dx * dx + dy * dy <= radius * radius) {
                    counts[pixels[newX][newY].colour.id]++;
                }
            }
        }
        // Find largest
        int firstLargestId = -1,
            firstLargestCount = Integer.MIN_VALUE,
            secondLargestId = -1,
            secondLargestCount = Integer.MIN_VALUE,
            nonZeroCount = 0;
        for (int i = 0; i < counts.length; i++) {
            if (counts[i] > 0) {
                nonZeroCount++;
            }
            if (counts[i] > firstLargestCount) {
                firstLargestId = i;
                firstLargestCount = counts[i];
            }
        }
        
        // If there are more than two colours in the area, then reject this.
        // The radius should be made smaller to make the gradient sharper.
        if (nonZeroCount > 2 && radius > 1) return null;
        
        
        // If the radius is more than minimum, and there are two colours in
        // the area, do a bit of extra processing.
        if (radius > 1 && nonZeroCount == 2) {
            
            // Search an area 4 pixels bigger than the radius.
            int[] secondaryCounts = new int[ColourDef.defs.length];
            for (int dy = -radius-4; dy <= radius+4; dy++) {
                for (int dx = -radius-4; dx <= radius+4; dx++) {
                    int newX = x + dx, newY = y + dy;
                    if (newX >= 0 && newY >= 0 && newX < pixImageWidth && newY < pixImageHeight &&
                        (dx * dx + dy * dy <= (radius+4) * (radius+4))) {
                        secondaryCounts[pixels[newX][newY].colour.id]++;
                    }
                }
            }
            
            // Count how many distinct colours exist in this area.
            int nonZeroCountSec = 0;
            for (int i = 0; i < secondaryCounts.length; i++) {
                if (secondaryCounts[i] > 0) {
                    nonZeroCountSec++;
                }
            }
            
            // If more than two distinct colours exist in this area (4 pixels bigger than radius),
            // then reject this radius. This has the following effect: When radiuses are forced
            // smaller due to the presence of more than 2 colours, radiuses are rejected until
            // the radius is 4 pixels smaller than is required to only have 2 colours.
            // This allows at least two "buffer" pixels, with only a single colour to be present.
            // Doing this allows the normalisation (below) to work properly.
            if (nonZeroCountSec > 2) return null;
        }
    
        // Find second largest
        counts[firstLargestId] = Integer.MIN_VALUE;
        for (int i = 0; i < counts.length; i++) {
            if (counts[i] > secondLargestCount) {
                secondLargestId = i;
                secondLargestCount = counts[i];
            }
        }
        if (secondLargestCount == 0) secondLargestId = firstLargestId;
        return new WeightedColPair(ColourDef.defs[firstLargestId], firstLargestCount, ColourDef.defs[secondLargestId], secondLargestCount);
    }
}


public class ColourConv {
    static long lastProgressTime = 0;
    private static void printProgress(float percent) {
        long currentTime = System.currentTimeMillis();
        if (currentTime - lastProgressTime > 100) {
            lastProgressTime = currentTime;
            System.out.printf("    %.2f%%...\n", percent);
        }
    }
    
    public static BufferedImage hFlipImage(BufferedImage img) {
        int width = img.getWidth(),
            height = img.getHeight();
        BufferedImage ret = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);
        
        for (int iy = 0; iy < height; iy++) {
            for (int ix = 0; ix < width; ix++) {
                ret.setRGB(width - ix - 1, iy, img.getRGB(ix, iy));
            }
        }
        
        return ret;
    }
    
    public static void main(String[] args) throws Exception {
        ColourDef.genColourDefs();
        
        // Load (and horizontally flip) input image.
        System.out.println("Reading images...");
        BufferedImage colourImage = ImageIO.read(new File("coloursIn.png"));
        colourImage = hFlipImage(colourImage);
        int width = colourImage.getWidth(),
            height = colourImage.getHeight();
        
        // Build array of pixels.
        System.out.println("Building pixel data...");
        Pix[][] pixels = new Pix[width][];
        for (int i = 0; i < height; i++) { pixels[i] = new Pix[height]; }
        
        // Fill array of pixels.
        for (int iy = 0; iy < height; iy++) {
            for (int ix = 0; ix < width; ix++) {
                pixels[ix][iy] = new Pix(ix, iy, new Color(colourImage.getRGB(ix, iy), true));
                printProgress(((((float)iy) * width + ((float)ix)) * 100) / (width * height));
            }
        }
        
        // Generating colour pairs.
        System.out.println("Generating colour pairs...");
        for (int iy = 0; iy < height; iy++) {
            for (int ix = 0; ix < width; ix++) {
                pixels[ix][iy].generateColourPair(pixels, width, height);
                printProgress(((((float)iy) * width + ((float)ix)) * 100) / (width * height));
            }
        }
        
        // Normalising colour pairs.
        System.out.println("Normalising colour pairs (first)...");
        for (int iy = 0; iy < height; iy++) {
            for (int ix = 0; ix < width; ix++) {
                pixels[ix][iy].firstLevelNormalise(pixels, width, height);
                printProgress(((((float)iy) * width + ((float)ix)) * 100) / (width * height));
            }
        }
        for (int iy = 0; iy < height; iy++) {
            for (int ix = 0; ix < width; ix++) {
                pixels[ix][iy].applyNormalised();
            }
        }
        
        // Normalising colour pairs.
        System.out.println("Normalising colour pairs (second)...");
        for (int iy = 0; iy < height; iy++) {
            for (int ix = 0; ix < width; ix++) {
                pixels[ix][iy].secondLevelNormalise(pixels, width, height);
                printProgress(((((float)iy) * width + ((float)ix)) * 100) / (width * height));
            }
        }
        for (int iy = 0; iy < height; iy++) {
            for (int ix = 0; ix < width; ix++) {
                pixels[ix][iy].applyNormalised();
            }
        }
        
        // Getting output from pixels.
        System.out.println("Extracting output...");
        BufferedImage outputImage = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);
        for (int iy = 0; iy < height; iy++) {
            for (int ix = 0; ix < width; ix++) {
                outputImage.setRGB(ix, iy, pixels[ix][iy].getOutput().getRGB());
                printProgress(((((float)iy) * width + ((float)ix)) * 100) / (width * height));
            }
        }
        
        // Save image.
        System.out.println("Saving output image...");
        ImageIO.write(outputImage, "png", new File("coloursOut.png"));
    }
}
