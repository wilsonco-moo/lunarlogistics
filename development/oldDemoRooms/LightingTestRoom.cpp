#include <GL/gl3w.h> // Include first

#include "LightingTestRoom.h"

#include <bullet3/BulletDynamics/Dynamics/btRigidBody.h>
#include <wool/window/base/WindowBase.h>
#include <glm/gtc/matrix_transform.hpp>
#include <awe/util/ConversionUtil.h>
#include <glm/gtc/type_ptr.hpp>
#include <awe/util/Types.h>
#include <awe/util/Util.h>
#include <GL/freeglut.h>
#include <iostream>
#include <cstdlib>
#include <cmath>
#include <list>


namespace lunarlogistics {
    
    LightingTestRoom::LightingTestRoom(void) :
        lastRemaining((unsigned int)-1),
        shaderProgram(buildDefaultShader()),
        viewProjectionUniformLocation(glGetUniformLocation(shaderProgram, "viewProjectionTransform")),
        threadQueue(),
        
        // Physics stuff
        physicsWorld(),
        
        // Awe stuff
        textureManager(&threadQueue, 512),
        modelManager(&threadQueue),
        assetManager(&threadQueue, &textureManager, &modelManager, shaderProgram),
        lightingManager(shaderProgram, awe::GLuvec3(32, 32, 4), awe::GLvec3(0.0f, 0.0f, 0.0f), awe::GLvec3(4.0f, 4.0f, 0.5f)),
        camera(&physicsWorld, awe::GLvec3(2.0f, 2.0f, 1.7f), 0.0f, 0.0f, 0.1f),
        worldManager(&threadQueue, &assetManager, &lightingManager, &physicsWorld),
        
        timeStep(0.0f),
        grabbedInstance(NULL),
        positionDiff(0,0,0),
        startHDir(0.0f),
        startVDir(0.0f),
        rotating(false) {
            
        camera.getCharacterController().mJumpSpeed = 1.7f;
        camera.getCharacterController().mSpeedDamping = 0.05f;
        camera.getCharacterController().mWalkAccel = 100.0f;
        camera.getCharacterController().mMaxLinearVelocity2 = 3.0f * 3.0f;
        
        worldManager.loadFromFile("assets/simpleWorld.xml");
    }
    
    LightingTestRoom::~LightingTestRoom(void) {
        glDeleteShader(shaderProgram);
    }
    
    
    void LightingTestRoom::compileAndCheckShader(const std::string & name, GLuint shader) {
        // Try to compile the shader.
        glCompileShader(shader);
        
        // If there is a log, print it on stderr.
        GLint len;
        glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &len);
        if (len != 0) {
            std::string str(len, ' ');
            glGetShaderInfoLog(shader, len, NULL, &str[0]);
            std::cerr << str;
        }
        
        // If it didn't compile, complain.
        GLint isCompiled = 0;
        glGetShaderiv(shader, GL_COMPILE_STATUS, &isCompiled);
        if (isCompiled == GL_FALSE) {
            std::cerr << "ERROR: Failed to compile " << name << ".\n";
            exit(EXIT_FAILURE);
        }
    }
    
    void LightingTestRoom::linkAndCheckProgram(const std::string & name, GLuint program) {
        // Try to link the shader.
        glLinkProgram(program);
        
        // If there is a log, print it on stderr.
        GLint len;
        glGetProgramiv(program, GL_INFO_LOG_LENGTH, &len);
        if (len != 0) {
            std::string str(len, ' ');
            glGetProgramInfoLog(program, len, NULL, &str[0]);
            std::cerr << str;
        }
        
        // If it didn't link, complain.
        GLint isLinked = 0;
        glGetProgramiv(program, GL_LINK_STATUS, &isLinked);
        if (isLinked == GL_FALSE) {
            std::cerr << "ERROR: Failed to link " << name << ".\n";
            exit(EXIT_FAILURE);
        }
    }
    
    GLuint LightingTestRoom::compileShaderProgram(const std::string & name, const char * vertexShaderSource, const char * geometryShaderSource, const char * fragmentShaderSource) {
        // Create and compile vertex shader.
        GLuint vertexShader = 0, geometryShader = 0, fragmentShader = 0;
        
        // Create and compile vertex shader.
        if (vertexShaderSource != NULL) {
            vertexShader = glCreateShader(GL_VERTEX_SHADER);
            glShaderSource(vertexShader, 1, &vertexShaderSource, NULL);
            compileAndCheckShader(name+": vertex shader", vertexShader);
        }
        
        // Create and compile geometry shader.
        if (geometryShaderSource != NULL) {
            geometryShader = glCreateShader(GL_GEOMETRY_SHADER);
            glShaderSource(geometryShader, 1, &geometryShaderSource, NULL);
            compileAndCheckShader(name+": geometry shader", geometryShader);
        }
        
        // Create and compile fragment shader.
        if (fragmentShaderSource != NULL) {
            fragmentShader = glCreateShader(GL_FRAGMENT_SHADER);
            glShaderSource(fragmentShader, 1, &fragmentShaderSource, NULL);
            compileAndCheckShader("fragment shader", fragmentShader);
        }
        
        // Create shader program, attach all shaders and link it.
        GLuint prog = glCreateProgram();
        if (vertexShaderSource != NULL)   glAttachShader(prog, vertexShader);
        if (geometryShaderSource != NULL) glAttachShader(prog, geometryShader);
        if (fragmentShaderSource != NULL) glAttachShader(prog, fragmentShader);
        linkAndCheckProgram("main shader program", prog);
        
        // Delete the compiled shaders: We don't need them any more since they
        // are part of the program.
        if (vertexShaderSource != NULL)   glDeleteShader(vertexShader);
        if (geometryShaderSource != NULL) glDeleteShader(geometryShader);
        if (fragmentShaderSource != NULL) glDeleteShader(fragmentShader);
        
        return prog;
    }
    
    GLuint LightingTestRoom::buildDefaultShader(void) {
        return compileShaderProgram(
            "Main default shader",
            #include <awe/defaultShader/vertex.h>
            , NULL,
            #include <awe/defaultShader/fragment.h>
        );
    }
    
    void LightingTestRoom::init(void) {
    }
    
    void LightingTestRoom::step(void) {
        GLfloat elapsed = getWindowBase()->elapsed();
        
        // Clear colour buffer with colour, clear depth and stencil buffers.
        GLfloat colour[4] = { 0.56f, 0.92f, 1.0f, 1.0f };
        glClearBufferfv(GL_COLOR, 0, colour);
        glClearBufferfi(GL_DEPTH_STENCIL, 0, 1.0f, 0);
        
        // Do load operations and print remaining.
        unsigned int remaining = 0;
        worldManager.step();
        remaining += textureManager.step(3);
        remaining += modelManager.step();
        remaining += assetManager.step(3);
        if (remaining != lastRemaining) {
            lastRemaining = remaining;
            if (remaining != 0) {
                std::cout << "REMAINING: " << remaining << "\n";
            }
        }
        
        // Update grabbing. Make sure this is done before updating the camera position.
        updateGrabbing();
        
        // If loading is finished, update the physics.
        if (remaining == 0) {
            physicsWorld.step(elapsed);
        }
        
        //light.setPosition(awe::GLvec3(2 + sin(timeStep), 2 + sin(timeStep*0.25), 0.5));
        //light.update();
        
        timeStep += 0.8f * elapsed;
        timeStep = fmod(timeStep, 8.0f * M_PI);
        
        // Bind the shader program.
        glUseProgram(shaderProgram);
        
        // Request that the camera updates it's position, and use this returned matrix as the view-projection transformation uniform.
        glUniformMatrix4fv(viewProjectionUniformLocation, 1, GL_FALSE, glm::value_ptr(camera.updateCameraPosition(getWindowBase()->elapsed())));
        
        // Draw all instances.
        assetManager.draw();
        
        // Unbind the shader program at the end.
        glUseProgram(0);
        
        glutSwapBuffers();
    }
    
    // ------------------------------ Instance grabbing ------------------------
    
    void LightingTestRoom::grabInstance(void) {
        awe::GLvec3 hitPoint;
        awe::Instance * raycastInstance = physicsWorld.raycastSingleInstance(hitPoint, camera.getPosition(), camera.getHDir(), camera.getVDir(), 8.0f);
        if (raycastInstance != NULL && !raycastInstance->isCollisionStatic()) {
            grabbedInstance = raycastInstance;
            startHDir = camera.getHDir();
            startVDir = camera.getVDir();
            positionDiff = grabbedInstance->getWorldCentreOfMass() - camera.getPosition();
            grabbedInstance->getRigidBody()->setDamping(0.9f, 0.9f); // linear, angular
            rotating = false;
            std::cout << "Grabbed " << grabbedInstance->getInstanceName() << '\n';
            
            btVector3 com = grabbedInstance->getRigidBody()->getCenterOfMassPosition();
            std::cout << "Position: " << com.getX() << ", " << com.getY() << ", " << com.getZ() << "\n";
        }
    }
    void LightingTestRoom::startRotatingInstance(void) {
        if (grabbedInstance != NULL) {
            rotating = true;
        }
    }    
    void LightingTestRoom::stopRotatingInstance(void) {
        rotating = false;
    }
    void LightingTestRoom::releaseInstance(void) {
        if (grabbedInstance != NULL) {
            std::cout << "Released " << grabbedInstance->getInstanceName() << '\n';
            grabbedInstance->getRigidBody()->setDamping(0.0f, 0.0f); // linear, angular
            grabbedInstance = NULL;
        }
    }
    void LightingTestRoom::updateGrabbing(void) {
        if (grabbedInstance != NULL) {
            // Don't accumulate forces from frame to frame.
            grabbedInstance->getRigidBody()->clearForces();
            
            // Don't let the rigid body sleep while we're moving it.
            grabbedInstance->getRigidBody()->activate();
            
            if (rotating) {
                // Get the x and y mousediff from the camera, and cancel it so the camera look direction doesn't change.
                int mouseDiffX = camera.getMouseDiffX(),
                    mouseDiffY = camera.getMouseDiffY();
                camera.setMouseDiffX(0);
                camera.setMouseDiffY(0);
                
                // Work out the torque force and scale it to the instance's mass.
                GLfloat torqueForce = grabbedInstance->getRigidBody()->getMass() * 0.5f;
                // Add a torque appropriate to the camera look direction, and how much the mouse moved.
                grabbedInstance->getRigidBody()->applyTorque(btVector3(-sin(camera.getHDir()) * ((GLfloat)mouseDiffY) * torqueForce,
                                                                       -cos(camera.getHDir()) * ((GLfloat)mouseDiffY) * torqueForce,
                                                                       ((GLfloat)mouseDiffX) * torqueForce));
            }
            
            // Work out target position.
            awe::GLvec3 targetPos = camera.getPosition() + awe::GLvec3(
                glm::rotate( // Outer transformation done first (inner * outer)   (cancel hDir, rotate vDir, rotate hDir)
                    glm::rotate(
                        glm::rotate(
                            awe::GLmat4(1.0f),
                            -camera.getHDir(),
                            awe::GLvec3(0,0,1)
                        ),
                        camera.getVDir() - startVDir,
                        awe::GLvec3(0,1,0)
                    ),
                    startHDir,
                    awe::GLvec3(0,0,1)
                ) * awe::GLvec4(positionDiff, 0.0f)
            );
            // Apply a force towards that target position. Scale the force to the instance's mass.
            awe::GLvec3 force = targetPos - grabbedInstance->getWorldCentreOfMass();
            force *= (grabbedInstance->getRigidBodyMass() * 60.0f);
           
            grabbedInstance->getRigidBody()->applyCentralForce(awe::ConversionUtil::glmToBulletVec3(force));
        }
    }
    // -------------------------------------------------------------------------
    
    void LightingTestRoom::reshape(GLsizei width, GLsizei height) {
        /*glViewport(0.0f, 0.0f, width, height);
        // 50 degrees fovy
        GLfloat aspect = ((GLfloat)width) / ((GLfloat)height);
        projectionTransform = glm::perspective(0.872664626f, aspect, 0.1f, 1000.0f);*/
        camera.onResize(width, height);
    }
    
    void LightingTestRoom::end(void) {}
    void LightingTestRoom::keyNormal(unsigned char key, int x, int y) {
        camera.onKeyPress(key);
    }
    void LightingTestRoom::keyNormalRelease(unsigned char key, int x, int y) {
        camera.onKeyRelease(key);
    }
    void LightingTestRoom::keySpecial(int key, int x, int y) {
        camera.onKeyPressSpecial(key);
    }
    void LightingTestRoom::keySpecialRelease(int key, int x, int y) {
        camera.onKeyReleaseSpecial(key);
    }
    void LightingTestRoom::mouseEvent(int button, int state, int x, int y) {
        if (button == GLUT_LEFT_BUTTON) {
            if (state == GLUT_DOWN) {
                // Pick up instances when we press left mouse.
                grabInstance();
            } else if (state == GLUT_UP) {
                // Put down any grabbed instance when we release left mouse.
                releaseInstance();
            }
        } else if (button == GLUT_RIGHT_BUTTON) {
            if (state == GLUT_DOWN) {
                // Start rotating instance when we press right mouse.
                startRotatingInstance();
            } else if (state == GLUT_UP) {
                // Put down any grabbed instance when we release right mouse.
                stopRotatingInstance();
            }
        }
    }
    void LightingTestRoom::mouseMove(int x, int y) {
        camera.onMouseMove(x, y);
    }
    void LightingTestRoom::mouseDrag(int x, int y) {
        camera.onMouseMove(x, y);
    }
    bool LightingTestRoom::shouldDeleteOnRoomEnd(void) { return true; }

}
