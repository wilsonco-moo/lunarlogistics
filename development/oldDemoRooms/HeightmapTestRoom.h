#ifdef LUNARLOGISTICS_HEIGHTMAPTESTROOM_H_
#define LUNARLOGISTICS_HEIGHTMAPTESTROOM_H_

#include <wool/room/base/RoomBase.h>
#include <awe/heightmap/HeightMap.h>
#include <wool/texture/Texture.h>
#include <glm/mat4x4.hpp>
#include <unordered_map>
#include <GL/gl.h>
#include <array>

namespace threading {
    class ThreadQueue;
}

namespace lunarlogistics {

    class HeightmapTestRoom : public wool::RoomBase {
    private:
        
        // A thread queue for asynchronous operation.
        threading::ThreadQueue * threadQueue;
        
        // Projection and model-view transforms.
        glm::mat4 projectionTransform, modelViewTransform;
        
        // Our shader program. This is set up in compileShaders.
        GLuint shaderProgram;
        
        // The size of the world.
        GLuint worldSize;
        
        // Uniform locations for matrices and texture size.
        GLuint mvpTransformMatrixUniform,
               worldSizeUniform,
               worldHeightUniform;
        
        // For camera movement.
        GLfloat timeStep;
        
        // For terrain height adjustment.
        GLfloat totalTimeElapsed;
        
        // Our heightmap.
        awe::HeightMap heightmap;
        
    public:
        HeightmapTestRoom(threading::ThreadQueue * threadQueue);
        virtual ~HeightmapTestRoom(void);
    private:
    
        static void compileAndCheckShader(const char * name, GLuint shader);
        static void linkAndCheckProgram(const char * name, GLuint program);
    
        void initGl3w(void);
        void compileShaders(void);
        void setupUniforms(void);
        
    public:
        virtual void init(void) override;
        virtual void step(void) override;
        virtual void end(void) override;
        virtual void keyNormal(unsigned char key, int x, int y) override;
        virtual void keyNormalRelease(unsigned char key, int x, int y) override;
        virtual void keySpecial(int key, int x, int y) override;
        virtual void keySpecialRelease(int key, int x, int y) override;
        virtual void reshape(GLsizei width, GLsizei height) override;
        virtual void mouseEvent(int button, int state, int x, int y) override;
        virtual void mouseMove(int x, int y) override;
        virtual void mouseDrag(int x, int y) override;
        virtual bool shouldDeleteOnRoomEnd(void) override;

    };
}

#endif
