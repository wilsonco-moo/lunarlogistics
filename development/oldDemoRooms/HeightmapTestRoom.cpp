#ifdef COMMENTED_OUT
#include <GL/gl3w.h> // Include first

#include "HeightmapTestRoom.h"

#include <wool/texture/lodepng/lodepng.h>
#include <wool/window/base/WindowBase.h>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <GL/freeglut.h>
#include <glm/vec3.hpp>
#include <iostream>
#include <cstdlib>
#include <GL/gl.h>

namespace lunarlogistics {
    
    // ===================== Texture configuration stuff =======================
    
    /**
     * Defines, for each texture, the texture name and it's value.
     */
    #define TEXTURE_DEF   \
        X(clay,      31)  \
        X(darkRocks, 63)  \
        X(dryMud,    95)  \
        X(grass,     127) \
        X(ground,    159) \
        X(pebbles,   191) \
        X(redSand,   223) \
        X(sand,      255)
    #define TEXTURES_LOC "assets/"
    
    /**
     * Defines texture IDs for each texture.
     */
    class TexIds {
    public:
        #define X(name, value) name,
        enum { TEXTURE_DEF COUNT };
        #undef X
    };
    
    /**
     * Defines texture values for each texture.
     */
    class TexValues {
        #define X(name, value) name = value,
        enum { TEXTURE_DEF };
        #undef X
    };
    
    /**
     * A map from texture values to texture IDs.
     */
    #define X(name, value) { value, TexIds::name },
    const static std::unordered_map<unsigned int, unsigned int> textureIdMap = { TEXTURE_DEF };
    #undef X
    
    /**
     * A vector of texture filenames.
     */
    #define X(name, value) TEXTURES_LOC #name ".png",
    const static std::vector<std::string> textureFilenames = { TEXTURE_DEF };
    #undef X
    
    // =========================================================================
    
    void debugPrintFunc(GLenum source, GLenum type, GLuint id, GLenum severity,
                        GLsizei length, const GLchar * message, const void * userParam) {
        fprintf(stderr, "GL CALLBACK: %s type = 0x%x, severity = 0x%x, message = %s\n",
                (type == GL_DEBUG_TYPE_ERROR ? "** GL ERROR **" : ""),
                    type, severity, message);
    }
        
        
    
    HeightmapTestRoom::HeightmapTestRoom(threading::ThreadQueue * threadQueue) :
        threadQueue(threadQueue),
        projectionTransform(),
        modelViewTransform(),
        shaderProgram(0),
        worldSize(512),
        mvpTransformMatrixUniform(0),
        worldSizeUniform(0),
        timeStep(0.0f),
        totalTimeElapsed(0.0f),
        heightmap(threadQueue, 512, 512, "assets/heightmapHeight.png", "assets/heightmapTerrain.png", textureIdMap, textureFilenames) {
    }

    HeightmapTestRoom::~HeightmapTestRoom(void) {
        
    }
        
    void HeightmapTestRoom::compileAndCheckShader(const char * name, GLuint shader) {
        // Try to compile the shader.
        glCompileShader(shader);
        
        // If there is a log, print it on stderr.
        GLint len;
        glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &len);
        if (len != 0) {
            std::string str(len, ' ');
            glGetShaderInfoLog(shader, len, NULL, &str[0]);
            std::cerr << str;
        }
        
        // If it didn't compile, complain.
        GLint isCompiled = 0;
        glGetShaderiv(shader, GL_COMPILE_STATUS, &isCompiled);
        if (isCompiled == GL_FALSE) {
            std::cerr << "ERROR: Failed to compile " << name << ".\n";
            exit(EXIT_FAILURE);
        }
    }
    
    void HeightmapTestRoom::linkAndCheckProgram(const char * name, GLuint program) {
        // Try to link the shader.
        glLinkProgram(program);
        
        // If there is a log, print it on stderr.
        GLint len;
        glGetProgramiv(program, GL_INFO_LOG_LENGTH, &len);
        if (len != 0) {
            std::string str(len, ' ');
            glGetProgramInfoLog(program, len, NULL, &str[0]);
            std::cerr << str;
        }
        
        // If it didn't link, complain.
        GLint isLinked = 0;
        glGetProgramiv(program, GL_LINK_STATUS, &isLinked);
        if (isLinked == GL_FALSE) {
            std::cerr << "ERROR: Failed to link " << name << ".\n";
            exit(EXIT_FAILURE);
        }
    }

    void HeightmapTestRoom::init(void) {
        initGl3w();
        
        glEnable(GL_DEBUG_OUTPUT);
        glDebugMessageCallback(&debugPrintFunc, 0);
        
        compileShaders();
        setupUniforms();
        // Misc initialisation
        glEnable(GL_DEPTH_TEST);
    }
    
    void HeightmapTestRoom::initGl3w(void) {
        if (gl3wInit()) {
            std::cerr << "Failed to initialize gl3w.\n";
            exit(EXIT_FAILURE);
        }
        if (!gl3wIsSupported(3, 1)) {
            std::cerr << "OpenGL 3.1 not supported\n";
            exit(EXIT_FAILURE);
        }
    }
    
    void HeightmapTestRoom::compileShaders(void) {
        std::cout << "Compiling shaders...\n";
        
        // String literals for shader sources.
        const char * vertexShaderSource = 
        #include <awe/heightmap/shader/vertex.h>
        ;
        const char * fragmentShaderSource = 
        #include <awe/heightmap/shader/fragment.h>
        ;
        
        // Create and compile vertex shader.
        GLuint vertexShader = glCreateShader(GL_VERTEX_SHADER);
        glShaderSource(vertexShader, 1, &vertexShaderSource, NULL);
        compileAndCheckShader("vertex shader", vertexShader);
        
        // Create and compile fragment shader.
        GLuint fragmentShader = glCreateShader(GL_FRAGMENT_SHADER);
        glShaderSource(fragmentShader, 1, &fragmentShaderSource, NULL);
        compileAndCheckShader("fragment shader", fragmentShader);
        
        // Create shader program, attach both shaders and link it.
        shaderProgram = glCreateProgram();
        glAttachShader(shaderProgram, vertexShader);
        glAttachShader(shaderProgram, fragmentShader);
        linkAndCheckProgram("main shader program", shaderProgram);
        
        // Delete the compiled shaders: We don't need them any more since they
        // are part of the program.
        glDeleteShader(vertexShader);
        glDeleteShader(fragmentShader);
        std::cout << "Done.\n";
    }
    
    void HeightmapTestRoom::setupUniforms(void) {
        mvpTransformMatrixUniform = glGetUniformLocation(shaderProgram, "viewProjectionTransform");
        worldSizeUniform = glGetUniformLocation(shaderProgram, "worldSize");
        worldHeightUniform = glGetUniformLocation(shaderProgram, "worldHeight");
    }

    void HeightmapTestRoom::step(void) {
        
        // Clear colour buffer with colour, clear depth and stencil buffers.
        GLfloat colour[4] = { 0.56f, 0.92f, 1.0f, 1.0f };
        glClearBufferfv(GL_COLOR, 0, colour);
        glClearBufferfi(GL_DEPTH_STENCIL, 0, 1.0f, 0);
        
        // Use the shader program.
        glUseProgram(shaderProgram);
        
        // Update the look matrix.
        timeStep += 0.3f * getWindowBase()->elapsed();
        timeStep = fmod(timeStep, 2.0f * M_PI);
        modelViewTransform = glm::lookAt(
            glm::vec3(256 * cos(timeStep) + 256, 256 * -sin(timeStep) + 256, 100),
            glm::vec3(256,256,0), 
            glm::vec3(0, 0, 1)
        );
        
        /*
        modelViewTransform = glm::lookAt(
            glm::vec3(256, 256, 600),
            glm::vec3(256, 256, 0), 
            glm::vec3(0, -1, 0)
        );
        */
        
        // Smoothly increase world height from 0 to THEIGHTMAX, during the time between THEIGHT1 and THEIGHT2.
        #define THEIGHT1 10.0f
        #define THEIGHT2 15.0f
        #define THEIGHTMAX 48.0f
        if (totalTimeElapsed < THEIGHT1) {
            totalTimeElapsed += getWindowBase()->elapsed();
            glUniform1f(worldHeightUniform, 0.0f);
        } else {
            if (totalTimeElapsed < THEIGHT2) {
                glUniform1f(worldHeightUniform,
                    THEIGHTMAX - THEIGHTMAX * pow(2, -((totalTimeElapsed - THEIGHT1)/(THEIGHT2-THEIGHT1)) * 8.0f)
                );
                totalTimeElapsed += getWindowBase()->elapsed();
            } else {
                glUniform1f(worldHeightUniform, THEIGHTMAX);
            }
        }
        
        // Tell the shader about the new model view projection matrix.
        glm::mat4 mvpTransform = projectionTransform * modelViewTransform;
        glUniformMatrix4fv(mvpTransformMatrixUniform, 1, GL_FALSE, glm::value_ptr(mvpTransform));
        
        // Tell the shader about the current world size.
        glUniform1i(worldSizeUniform, worldSize);
        
        // Draw the heightmap.
        unsigned int remaining = heightmap.step(10, shaderProgram);
        if (remaining != 0) {
            //std::cout << remaining << " load remaining...\n";
        }
        
        glutSwapBuffers();
    }
    
    void HeightmapTestRoom::reshape(GLsizei width, GLsizei height) {
        glViewport(0.0f, 0.0f, width, height);
        GLfloat aspect = ((GLfloat)width) / ((GLfloat)height);
        // 50 degrees fovy
        projectionTransform = glm::perspective(0.872664626f, aspect, 0.1f, 1000.0f);
    }
    
    void HeightmapTestRoom::end(void) {
    }
    void HeightmapTestRoom::keyNormal(unsigned char key, int x, int y) {}
    void HeightmapTestRoom::keyNormalRelease(unsigned char key, int x, int y) {}
    void HeightmapTestRoom::keySpecial(int key, int x, int y) {}
    void HeightmapTestRoom::keySpecialRelease(int key, int x, int y) {}
    void HeightmapTestRoom::mouseEvent(int button, int state, int x, int y) {}
    void HeightmapTestRoom::mouseMove(int x, int y) {}
    void HeightmapTestRoom::mouseDrag(int x, int y) {}
    bool HeightmapTestRoom::shouldDeleteOnRoomEnd(void) { return false; }

}
#endif
