#ifndef LUNARLOGISTICS_DEMOROOM_H_
#define LUNARLOGISTICS_DEMOROOM_H_

#include <wool/room/base/RoomBase.h>
#include <wool/texture/Texture.h>
#include <glm/mat4x4.hpp>
#include <unordered_map>
#include <GL/gl.h>
#include <array>

/**
 * Colour format:
 * RED:   Height (0-255) -> normalised float
 * GREEN: Colour mix (0-255) -> normalised float
 * BLUE:  Terrain type 1
 * ALPHA: Terrain type 2
 */

// ID, name, colourValue
#define TEXTURE_DEF      \
    X(0, clay,      31)  \
    X(1, darkRocks, 63)  \
    X(2, dryMud,    95)  \
    X(3, grass,     127) \
    X(4, ground,    159) \
    X(5, pebbles,   191) \
    X(6, redSand,   223) \
    X(7, sand,      255)
#define TEXTURES_COUNT 8
#define TEXTURES_LOC "assets/"

namespace lunarlogistics {

    class DemoRoom : public wool::RoomBase {
    private:
        const static std::unordered_map<uint32_t, GLubyte> texColToId;
        
        // This contains all the data required for each vertex.
        class VertexInfo {
        public:
            GLubyte height,
                    mix,
                    textureId1,
                    textureId2;
        };
        
        // Make sure the compiler hasn't done anything funny with the layout of this
        // class: We need this to talk to the graphics card.
        static_assert(sizeof(GLubyte) == 1 &&
                      sizeof(VertexInfo) == 4 &&
                      offsetof(VertexInfo, height) == 0 &&
                      offsetof(VertexInfo, mix) == 1 &&
                      offsetof(VertexInfo, textureId1) == 2 &&
                      offsetof(VertexInfo, textureId2) == 3);
        
        // The vertex array object for feeding data to the shader, from the buffer.
        GLuint vao;
        
        // A buffer.
        GLuint vertexDataBuffer, vertexIndexBuffer;
        
        // Projection and model-view transforms.
        glm::mat4 projectionTransform, modelViewTransform;
        
        // Our shader program. This is set up in compileShaders.
        GLuint shaderProgram;
        
        // The size of the world.
        GLuint worldSize;
        
        // The number of vertex indices to draw. (Should be (2*worldSize+1)*(worldSize-1)).
        GLsizei vertexIndices;
        
        // The texture array for the heightmap.
        GLuint textureArr;
        
        // Uniform locations for matrices and texture size.
        GLuint mvpTransformMatrixUniform,
               worldSizeUniform,
               worldHeightUniform;
        
        // For camera movement.
        GLfloat timeStep;
        
        // For terrain height adjustment.
        GLfloat totalTimeElapsed;
        
    public:
        DemoRoom(void);
        virtual ~DemoRoom(void);
    private:
        static GLubyte colourToTexId(unsigned int x, unsigned int y, uint32_t colourValue);
        static void compileAndCheckShader(const char * name, GLuint shader);
        static void linkAndCheckProgram(const char * name, GLuint program);
    
        void initGl3w(void);
        void compileShaders(void);
        void setupUniforms(void);
        void loadWorldTextures(void);
        void loadWorldBuffer(void);
        void setupVertexArray(void);
        
    public:
        virtual void init(void) override;
        virtual void step(void) override;
        virtual void end(void) override;
        virtual void keyNormal(unsigned char key, int x, int y) override;
        virtual void keyNormalRelease(unsigned char key, int x, int y) override;
        virtual void keySpecial(int key, int x, int y) override;
        virtual void keySpecialRelease(int key, int x, int y) override;
        virtual void reshape(GLsizei width, GLsizei height) override;
        virtual void mouseEvent(int button, int state, int x, int y) override;
        virtual void mouseMove(int x, int y) override;
        virtual void mouseDrag(int x, int y) override;
        virtual bool shouldDeleteOnRoomEnd(void) override;

    };
}

#endif
