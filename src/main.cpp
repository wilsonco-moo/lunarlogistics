#include <GL/gl3w.h> // Include first

#include <wool/window/config/WindowConfig.h>
#include <wool/window/base/WindowBase.h>
#include <threading/ThreadQueue.h>
#include <GL/freeglut.h>
#include <iostream>
#include <cstdlib>

#include "lunarLogistics/InitRoom.h"


// Uncomment to run OpenAL test (also install libopenal-dev)
//#define DO_OPENAL_TEST

#ifdef DO_OPENAL_TEST
    #include <awe/audio/OpenALContext.h>
    #include <AL/alc.h>
    #include <AL/al.h>
    #include <cstddef>
    #include <cstdint>
    #include <thread>
    #include <chrono>

    #define DATA_SIZE  1024
    #define WAVELENGTH 128
    #define VOLUME     0.2f
    #define SAMPLERATE 8000
    #define PLAY_TIME  std::chrono::seconds(2)

    int testOpenAL(void) {
        awe::OpenALContext context;
        if (!context.loadedSuccessfully()) return EXIT_FAILURE;
        
        // Allocate buffer, fill it with data.
        ALuint buffer;
        alGenBuffers(1, &buffer);
        {
            int16_t data[DATA_SIZE];
            awe::OpenALContext::makeSineWave(data, DATA_SIZE, WAVELENGTH, VOLUME);
            alBufferData(buffer, AL_FORMAT_MONO16, data, DATA_SIZE * sizeof(int16_t), SAMPLERATE);
        }
        
        // Create a source to play our buffer, set properties: tell it to loop.
        ALuint source;
        alGenSources(1, &source);
        alSourcei(source, AL_BUFFER, buffer);
        alSourcei(source, AL_LOOPING, AL_TRUE);
        
        // Play, wait around, stop playing.
        alSourcePlay(source);
        std::this_thread::sleep_for(PLAY_TIME);
        alSourceStop(source);
        
        // Delete source then buffer.
        alDeleteSources(1, &source);
        alDeleteBuffers(1, &buffer);
        
        return EXIT_SUCCESS;
    }
#endif


int main(int argc, char * argv[]) {

    #ifdef DO_OPENAL_TEST
        testOpenAL();
    #endif

    {
        lunarlogistics::InitRoom * roomPtr = new lunarlogistics::InitRoom();
        wool::RoomBase & room = *roomPtr;
        
        //threading::ThreadQueue threadQueue;
        //lunarlogistics::HeightmapTestRoom room(&threadQueue);
        
        wool::WindowConfig config;
        config.windowTitle = (char *)"Lunar logistics";
        config.enableWindozeVsync = true;
        config.width = 640;
        config.height = 480;
        config.displayMode = GLUT_DOUBLE | GLUT_RGBA | GLUT_DEPTH;
        wool::WindowBase windowBase(&room, config);
        windowBase.enableFrameRateLogging = true;
        windowBase.start(&argc, argv);
        
        //room.end();
    }
    std::cout << "Program done.\n";
    return EXIT_SUCCESS;
}
