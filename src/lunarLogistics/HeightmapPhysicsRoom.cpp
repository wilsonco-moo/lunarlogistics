#include <GL/gl3w.h> // Include first

#include "HeightmapPhysicsRoom.h"

#include <bullet3/BulletDynamics/Dynamics/btRigidBody.h>
#include <awe/util/types/ConversionUtil.h>
#include <wool/window/base/WindowBase.h>
#include <glm/gtc/matrix_transform.hpp>
#include <awe/util/colour/Channels.h>
#include <rascUI/util/Bindings.h>
#include <awe/util/types/Types.h>
#include <glm/gtc/type_ptr.hpp>
#include <rascUI/util/Layout.h>
#include <awe/util/Util.h>
#include <GL/freeglut.h>
#include <algorithm>
#include <iostream>
#include <cstdlib>
#include <iomanip>
#include <sstream>
#include <string>
#include <cmath>
#include <list>

namespace lunarlogistics {
    
    // ===================== Texture configuration stuff =======================
    
    /**
     * Defines, for each texture, the texture name and it's value.
     */
    #define TEXTURE_DEF   \
        X(clay,      31)  \
        X(darkRocks, 63)  \
        X(dryMud,    95)  \
        X(grass,     127) \
        X(ground,    159) \
        X(pebbles,   191) \
        X(redSand,   223) \
        X(sand,      255)
    #define TEXTURES_LOC "assets/"
    
    /**
     * Defines texture IDs for each texture.
     */
    class TexIds {
    public:
        enum {
            #define X(name, value) name,
                TEXTURE_DEF
            #undef X
            #define X(name, value) name##Normals,
                TEXTURE_DEF
            #undef X
            COUNT
        };
    };
    
    /**
     * Defines texture values for each texture.
     */
    class TexValues {
        #define X(name, value) name = value,
        enum { TEXTURE_DEF };
        #undef X
    };
    
    /**
     * A map from texture values to texture IDs.
     */
    #define X(name, value) { value, TexIds::name },
    const static std::unordered_map<unsigned int, unsigned int> textureIdMap = { TEXTURE_DEF };
    #undef X
    
    /**
     * A vector of texture filenames.
     */
    const static std::vector<std::string> textureFilenames = {
        #define X(name, value) TEXTURES_LOC #name ".png",
            TEXTURE_DEF
        #undef X
        #define X(name, value) TEXTURES_LOC #name "Normals.png",
            TEXTURE_DEF
        #undef X
    };
    
    
    // ====================== UI layout stuff ==================================
    
    #define LAYOUT_THEME (&theme)

    #define QUALITY_BUTTON_WIDTH 50
    #define QUALITY_PANEL_LOC                                                                           \
        MOVE_X(-DISPLAY_PANEL_WIDTH - UI_BORDER,                                                        \
         PIN_R(QUALITY_BUTTON_WIDTH * ShaderQuality::COUNT + (ShaderQuality::COUNT - 1) * UI_BORDER,    \
          PIN_B(COUNT_OUTBORDER_Y(1),                                                                   \
           GEN_FILL                                                                                     \
        )))
    
    #define QUALITY_BUTTON_LOC(id)                  \
        BORDERTABLE_X(id, ShaderQuality::COUNT,     \
         SUB_BORDER(                                \
          QUALITY_PANEL_LOC                         \
        ))
    
    #define DISPLAY_PANEL_WIDTH  308
    #define DISPLAY_PANEL_HEIGHT COUNT_OUTBORDER_Y(DISPLAY_PANEL_ROW_COUNT)
    #define DISPLAY_PANEL_LOC           \
        PIN_R(DISPLAY_PANEL_WIDTH,      \
         PIN_B(DISPLAY_PANEL_HEIGHT,    \
          GEN_FILL                      \
        ))
    #define DISPLAY_PANEL_ROW_COUNT 2
    #define DISPLAY_PANEL_ROW_LOC(id)               \
        BORDERTABLE_Y(id, DISPLAY_PANEL_ROW_COUNT,  \
         SUB_BORDER(                                \
          DISPLAY_PANEL_LOC                         \
        ))
    
    #define GEAR_LABEL_WIDTH 78
    #define GEAR_LABEL_LOC   PIN_L(GEAR_LABEL_WIDTH, DISPLAY_PANEL_ROW_LOC(0))
    #define RPM_BAR_LOC      SUB_BORDERMARGIN_L(GEAR_LABEL_WIDTH, DISPLAY_PANEL_ROW_LOC(0))
    
    #define RESET_BUTTON_WIDTH 60
    #define FLIP_BUTTON_WIDTH  50
    #define RPM_PANEL_WIDTH    88
    
    #define RESET_BUTTON_LOC            \
        PIN_L(RESET_BUTTON_WIDTH,       \
         DISPLAY_PANEL_ROW_LOC(1)       \
        )
    
    #define FLIP_BUTTON_LOC                         \
        PIN_L(FLIP_BUTTON_WIDTH,                    \
         SUB_BORDERMARGIN_L(RESET_BUTTON_WIDTH,     \
          DISPLAY_PANEL_ROW_LOC(1)                  \
        ))
    
    #define RPM_PANEL_LOC                                                           \
        PIN_L(RPM_PANEL_WIDTH,                                                      \
         SUB_BORDERMARGIN_L(RESET_BUTTON_WIDTH + UI_BORDER + FLIP_BUTTON_WIDTH,     \
          DISPLAY_PANEL_ROW_LOC(1)                                                  \
        ))
    
    #define SPEEDOMETER_LOC                                                                                   \
        SUB_BORDERMARGIN_L(RESET_BUTTON_WIDTH + UI_BORDER + FLIP_BUTTON_WIDTH + UI_BORDER + RPM_PANEL_WIDTH,  \
         DISPLAY_PANEL_ROW_LOC(1)                                                                             \
        )
    
    // =========================================================================
    
    HeightmapPhysicsRoom::HeightmapPhysicsRoom(void) :
        instanceShaderProgram(awe::Util::compileShaderProgram(
            "default",
            #include <awe/defaultShader/vertex.glsl-out>
            , NULL,
            #include <awe/defaultShader/fragment.glsl-out>
        )),
        heightmapShaderProgram(0), // Assign to zero initially so it can be ignored when deleted.
                                   // This is assigned inside "switchShader".
        
        instanceVpUniformLoc(awe::Util::getUniformLocationChecked(instanceShaderProgram, "HeightmapPhysicsRoom constructor", "viewProjectionTransform")),
        
        threadQueue(),
        
        // Awe and physics stuff
        openALContext(),
        physicsWorld(),
        textureManager(&threadQueue, 512),
        modelManager(&threadQueue),
        assetManager(&threadQueue, &textureManager, &modelManager, instanceShaderProgram),
        lightingManager(awe::GLuvec3(32, 32, 4), awe::GLvec3(0.0f, 0.0f, 0.0f), awe::GLvec3(4.0f, 4.0f, 0.5f)),
        
        //camera(&physicsWorld, awe::GLvec3(2.0f + 250, 2.0f + 90, 1.7f + 20), 0.0f, 0.0f, 0.1f),
        #ifdef FREE_CAMERA_MODE
            camera(awe::GLvec3(2.0f + 148, 2.0f + 187, 1.7f + 26), glm::radians(40.0f), glm::radians(30.0f)),
        #else
            camera(NULL, 2.5f, 0.4f),
        #endif
        
        worldManager(&threadQueue, &assetManager, &lightingManager, &physicsWorld, "assets/simpleWorld.xml"),
        
        heightmap(&threadQueue, &physicsWorld,
                  512,   // map size
                  1.0f,  // grid spacing
                  123.484f, //108.071f, // map height
                  512,   // base texture size
                  "assets/heightmapHeight.png", "assets/heightmapTerrain.png", textureIdMap, textureFilenames),
        
        terrainScatter1(&threadQueue, &assetManager, "assets/scatter/scatter.xml",
                       &heightmap, "assets/terrainScatter.png",
                       1.0f, 2.5f,            // min scale, max scale
                       0.028577f, true,       // z offset, randomise rotations
                       7500, 1,               // particle count, seed
                       awe::Channels::green), // Channel in scatter texture
        
        terrainScatter2(&threadQueue, &assetManager, "assets/scatter/scatter2.xml",
                       &heightmap, "assets/terrainScatter.png",
                       1.0f, 4.0f,            // min scale, max scale
                       0.028481f, true,       // z offset, randomise rotations
                       5000, 2,               // particle count, seed
                       awe::Channels::red),   // Channel in scatter texture
        
        physicsTerrainScatter(&threadQueue, &assetManager, "assets/largerock1/rockl1.xml",
                       &heightmap, "assets/terrainScatter.png",
                       0.1f, 0.3f,            // min scale, max scale
                       1.639528f, true,       // z offset, randomise rotations
                       300, &physicsWorld, 3, // particle count, physics world, seed
                       awe::Channels::red),   // Channel in scatter texture
        
        timeStep(0.0f),
        grabbedInstance(NULL),
        positionDiff(0,0,0),
        startHDir(0.0f),
        startVDir(0.0f),
        rotating(false),
        
        basicCar(&assetManager, &physicsWorld, &worldManager, "lada"),
        engineSound(&threadQueue, "assets/basicCar/engineSound"),
        gauge(rascUI::Location(PIN_B(64, PIN_L(128, GEN_FILL)))),
        
        viewWidth(800),
        viewHeight(600),
        uiScaleExp(0.0f),
        uiScale(1.0f),
        theme(),
        topLevel(&theme, &rascUI::Bindings::defaultOpenGL, &uiScale, &uiScale, true),
        qualityBackPanel(rascUI::Location(QUALITY_PANEL_LOC)),
        qualitySeries(false),
        qualityButtons(),
        displayBackPanel(rascUI::Location(DISPLAY_PANEL_LOC)),
        gearPanel(rascUI::Location(GEAR_LABEL_LOC)),
        gearLabel(rascUI::Location(GEAR_LABEL_LOC)),
        rpmBar(rascUI::Location(RPM_BAR_LOC)),
        resetButton(rascUI::Location(RESET_BUTTON_LOC), "Reset",
            [this](GLfloat viewX, GLfloat viewY, int button) {
                if (button == topLevel.getMb()) basicCar.resetPosition();
            }
        ),
        flipButton(rascUI::Location(FLIP_BUTTON_LOC), "Flip",
            [this](GLfloat viewX, GLfloat viewY, int button) {
                if (button == topLevel.getMb()) basicCar.flip();
            }
        ),
        rpmPanel(rascUI::Location(RPM_PANEL_LOC)),
        rpmLabel(rascUI::Location(RPM_PANEL_LOC)),
        speedPanel(rascUI::Location(SPEEDOMETER_LOC)),
        speedLabel(rascUI::Location(SPEEDOMETER_LOC), "0.0 mph"),
        progressPopup(&theme) {
        
        // Add quality buttons, along with their actions.
        topLevel.add(&qualityBackPanel);    
        #define X(name)                                                                                                 \
            qualityButtons.emplace(&qualitySeries, rascUI::Location(QUALITY_BUTTON_LOC(ShaderQuality::name)), #name,    \
                [this](GLfloat viewX, GLfloat viewY, int button) {                                                      \
                    switchShader(ShaderQuality::name);                                                                  \
                }                                                                                                       \
            );                                                                                                          \
            topLevel.add(&qualityButtons.back());
        SHADER_QUALITY_DEF
        #undef X
        qualitySeries.setSelectedId(ShaderQuality::defaultQuality);
        
        // Add other UI stuff.
        topLevel.add(&gauge);
        topLevel.add(&displayBackPanel);
        topLevel.add(&gearPanel);
        topLevel.add(&gearLabel);
        topLevel.add(&rpmBar);
        topLevel.add(&resetButton);
        topLevel.add(&flipButton);
        topLevel.add(&rpmPanel);
        topLevel.add(&rpmLabel);
        topLevel.add(&speedPanel);
        topLevel.add(&speedLabel);
        topLevel.add(&progressPopup);
        progressPopup.show();
        
        lightingManager.registerShader(instanceShaderProgram);
        gearLabel.setText(basicCar.getGearbox().getGearName(basicCar.getGearbox().getCurrentGear()));
        
        // Switch to default quality shader.
        switchShader(ShaderQuality::defaultQuality);
        
        /*
        camera.getCharacterController().mJumpSpeed = 2.0f;
        camera.getCharacterController().mSpeedDamping = 0.05f;
        camera.getCharacterController().mWalkAccel = 100.0f;
        camera.getCharacterController().mMaxLinearVelocity2 = 4.0f * 4.0f;
        */
        
        #ifdef FREE_CAMERA_MODE
            camera.moveSpeed = 4.0f;
        #else
            camera.setCameraDistanceExp(2.0f);
        #endif
    }
    
    HeightmapPhysicsRoom::~HeightmapPhysicsRoom(void) {
        lightingManager.unregisterShader(instanceShaderProgram);
        lightingManager.unregisterShader(heightmapShaderProgram);
        glDeleteShader(instanceShaderProgram);
        glDeleteShader(heightmapShaderProgram);
    }
    
    void HeightmapPhysicsRoom::switchShader(int quality) {
        // Delete old shader.
        if (heightmapShaderProgram != 0) {
            lightingManager.unregisterShader(heightmapShaderProgram);
            glDeleteShader(heightmapShaderProgram);
        }
        
        // Select fragment shader based on quality.
        const char * fragmentShader;
        switch(quality) {
        case ShaderQuality::Low:
            fragmentShader =
                #include "heightmapFragmentShaders/lowQuality.glsl-out"
            ; break;
            
        case ShaderQuality::Med:
            fragmentShader =
                #include "heightmapFragmentShaders/mediumQuality.glsl-out"
            ; break;
            
        default: // high
            fragmentShader =
                #include "heightmapFragmentShaders/highQuality.glsl-out"
            ; break;
        }
        
        // Allocate new shader.
        heightmapShaderProgram = awe::Util::compileShaderProgram(
            "heightmap",
            #include <awe/heightmap/shader/vertex.glsl-out>
            , NULL, fragmentShader
        );

        // Get uniform locations and register with lighting manager.
        heightmapVpUniformLoc = awe::Util::getUniformLocationChecked(heightmapShaderProgram, "HeightmapPhysicsRoom switchShader", "viewProjectionTransform");
        heightmapCamPosUniformLoc = awe::Util::getUniformLocationChecked(heightmapShaderProgram, "HeightmapPhysicsRoom switchShader", "cameraPos");
        lightingManager.registerShader(heightmapShaderProgram);
    }
    
    void HeightmapPhysicsRoom::init(void) {
    }
    
    void HeightmapPhysicsRoom::step(void) {
        GLfloat elapsed = getWindowBase()->elapsed();
        
        // Clear colour buffer with colour, clear depth and stencil buffers.
        GLfloat colour[4] = { 0.56f, 0.92f, 1.0f, 1.0f };
        glClearBufferfv(GL_COLOR, 0, colour);
        glClearBufferfi(GL_DEPTH_STENCIL, 0, 1.0f, 0);
        
        // Do load operations and print remaining.
        unsigned int remaining = 0;
        worldManager.step();
        remaining += textureManager.step(3);
        remaining += modelManager.step();
        remaining += assetManager.step(3);
        
        // Update grabbing. Make sure this is done before updating the camera position.
        updateGrabbing();
        
        // If loading is finished, update the physics.
        if (remaining == 0 && heightmap.getIsLoaded()) {
            physicsWorld.step(elapsed);
        }
        
        basicCar.step(elapsed);
        
        // Update the speedometer display
        {
            std::stringstream stream;
            stream << std::fixed << std::setprecision(1) << (basicCar.getAverageDriveWheelSpeed() * awe::ConversionUtil::MS_TO_MPH) << " mph";
            std::string speedStr = stream.str();
            if (speedLabel.getText() != speedStr) {
                speedLabel.setText(speedStr);
                speedLabel.repaint();
            }
        }
        
        // Update the engine RPM display
        {
            GLfloat engineRPM  = basicCar.getEngineRPM(),
                    maxRPM     = basicCar.getMaxEngineRPM(),
                    proportion = std::min(1.0f, engineRPM / maxRPM);
            if (proportion != rpmBar.getProgress()) {
                rpmBar.setProgress(proportion);
                rpmBar.repaint();
            }
            bool engaged = (basicCar.getGearbox().isEngaged() && basicCar.getEngine().isClutchFullyEngaged());
            if (engaged != gearPanel.isActive()) {
                gearPanel.setActive(engaged);
                gearLabel.setActive(engaged);
            }
            std::string rpmDisplayText = std::to_string((int)std::floor(engineRPM)) + " rpm";
            if (rpmDisplayText != rpmLabel.getText()) {
                rpmLabel.setText(rpmDisplayText);
                rpmLabel.repaint();
            }
        }
        
        // Update the engine sound.
        engineSound.updateEngineSpeed(basicCar.getEngine().getRotationSpeed());

        
        //light.setPosition(awe::GLvec3(2 + sin(timeStep), 2 + sin(timeStep*0.25), 0.5));
        //light.update();
        
        timeStep += 0.8f * elapsed;
        timeStep = fmod(timeStep, 8.0f * M_PI);
        
        // Update the camera, get the new view-projection transformation.
        #ifndef FREE_CAMERA_MODE
            if (camera.getInstance() == NULL) {
                camera.setInstance(basicCar.getCarInstance());
            }
        #endif
        awe::GLmat4 vpTransform = camera.updateCameraPosition(getWindowBase()->elapsed());
        
        // Bind instance shader, update vp transform, draw assets.
        glUseProgram(instanceShaderProgram);
        glUniformMatrix4fv(instanceVpUniformLoc, 1, GL_FALSE, glm::value_ptr(vpTransform));
        assetManager.draw();
        remaining += terrainScatter1.step();
        remaining += terrainScatter2.step();
        remaining += physicsTerrainScatter.step();
        
        // Bind heightmap shader, update vp transform, draw assets.
        glUseProgram(heightmapShaderProgram);
        glUniformMatrix4fv(heightmapVpUniformLoc, 1, GL_FALSE, glm::value_ptr(vpTransform));
        awe::GLvec3 eyepoint = camera.calculateEyepoint();
        glUniform3fv(heightmapCamPosUniformLoc, 1, glm::value_ptr(eyepoint));
        remaining += heightmap.step(10, heightmapShaderProgram);
        glUseProgram(0);
        
        
        glDisable(GL_DEPTH_TEST);
        glDisable(GL_CULL_FACE);
        topLevel.bufferedDraw(rascUI::Rectangle(0,0,viewWidth,viewHeight), 0.1);
        glEnable(GL_DEPTH_TEST);
        glEnable(GL_CULL_FACE);
        
        
        // Swap buffers now drawing is done.
        glutSwapBuffers();
        
        // Tell the progress popup about the remaining things to load.
        progressPopup.updateRemaining(remaining);
    }
    
    // ------------------------------ Instance grabbing ------------------------
    
    void HeightmapPhysicsRoom::grabInstance(void) {
        awe::GLvec3 hitPoint;
        awe::Instance * raycastInstance = physicsWorld.raycastSingleInstance(hitPoint, camera.getPosition(), camera.getHDir(), camera.getVDir(), 8.0f);
        if (raycastInstance != NULL && !raycastInstance->isCollisionStatic()) {
            grabbedInstance = raycastInstance;
            startHDir = camera.getHDir();
            startVDir = camera.getVDir();
            positionDiff = grabbedInstance->getWorldCentreOfMass() - camera.getPosition();
            grabbedInstance->getRigidBody()->setDamping(0.9f, 0.9f); // linear, angular
            rotating = false;
            std::cout << "Grabbed " << grabbedInstance->getInstanceName() << '\n';
            
            btVector3 com = grabbedInstance->getRigidBody()->getCenterOfMassPosition();
            std::cout << "Position: " << com.getX() << ", " << com.getY() << ", " << com.getZ() << "\n";
        }
    }
    void HeightmapPhysicsRoom::startRotatingInstance(void) {
        if (grabbedInstance != NULL) {
            rotating = true;
        }
    }    
    void HeightmapPhysicsRoom::stopRotatingInstance(void) {
        rotating = false;
    }
    void HeightmapPhysicsRoom::releaseInstance(void) {
        if (grabbedInstance != NULL) {
            std::cout << "Released " << grabbedInstance->getInstanceName() << '\n';
            grabbedInstance->getRigidBody()->setDamping(0.0f, 0.0f); // linear, angular
            grabbedInstance = NULL;
        }
    }
    void HeightmapPhysicsRoom::updateGrabbing(void) {
        if (grabbedInstance != NULL) {
            // Don't accumulate forces from frame to frame.
            grabbedInstance->getRigidBody()->clearForces();
            
            // Don't let the rigid body sleep while we're moving it.
            grabbedInstance->getRigidBody()->activate();
            
            if (rotating) {
                // Get the x and y mousediff from the camera, and cancel it so the camera look direction doesn't change.
                int mouseDiffX = camera.getMouseDiffX(),
                    mouseDiffY = camera.getMouseDiffY();
                camera.setMouseDiffX(0);
                camera.setMouseDiffY(0);
                if (mouseDiffX != 0 || mouseDiffY != 0) {
                    camera.warpMouseToCentre();
                }
                
                // Work out the torque force and scale it to the instance's mass.
                GLfloat torqueForce = grabbedInstance->getRigidBody()->getMass() * 0.5f;
                // Add a torque appropriate to the camera look direction, and how much the mouse moved.
                grabbedInstance->getRigidBody()->applyTorque(btVector3(-sin(camera.getHDir()) * ((GLfloat)mouseDiffY) * torqueForce,
                                                                       -cos(camera.getHDir()) * ((GLfloat)mouseDiffY) * torqueForce,
                                                                       ((GLfloat)mouseDiffX) * torqueForce));
            }
            
            // Work out target position.
            awe::GLvec3 targetPos = camera.getPosition() + awe::GLvec3(
                glm::rotate( // Outer transformation done first (inner * outer)   (cancel hDir, rotate vDir, rotate hDir)
                    glm::rotate(
                        glm::rotate(
                            awe::GLmat4(1.0f),
                            -camera.getHDir(),
                            awe::GLvec3(0,0,1)
                        ),
                        camera.getVDir() - startVDir,
                        awe::GLvec3(0,1,0)
                    ),
                    startHDir,
                    awe::GLvec3(0,0,1)
                ) * awe::GLvec4(positionDiff, 0.0f)
            );
            // Apply a force towards that target position. Scale the force to the instance's mass.
            awe::GLvec3 force = targetPos - grabbedInstance->getWorldCentreOfMass();
            force *= (grabbedInstance->getRigidBodyMass() * 60.0f);
           
            grabbedInstance->getRigidBody()->applyCentralForce(awe::ConversionUtil::glmToBulletVec3(force));
        }
    }
    // -------------------------------------------------------------------------
    
    void HeightmapPhysicsRoom::reshape(GLsizei width, GLsizei height) {
        /*glViewport(0.0f, 0.0f, width, height);
        // 50 degrees fovy
        GLfloat aspect = ((GLfloat)width) / ((GLfloat)height);
        projectionTransform = glm::perspective(0.872664626f, aspect, 0.1f, 1000.0f);*/
        camera.onResize(width, height);
        viewWidth = width;
        viewHeight = height;
    }
    
    void HeightmapPhysicsRoom::end(void) {}
    void HeightmapPhysicsRoom::keyNormal(unsigned char key, int x, int y) {
        if (topLevel.keyPress(key, false) && !progressPopup.isOpen()) {
            camera.onKeyPress(key);
            basicCar.onKeyPress(key, false);
            
            // Update the gear display if pressing the key has changed gear.
            const std::string & gearName = basicCar.getGearbox().getGearName(basicCar.getGearbox().getCurrentGear());
            if (gearName != gearLabel.getText()) {
                gearLabel.setText(gearName);
                gearLabel.repaint();
            }
        }
    }
    void HeightmapPhysicsRoom::keyNormalRelease(unsigned char key, int x, int y) {
        if (topLevel.keyRelease(key, false) && !progressPopup.isOpen()) {
            camera.onKeyRelease(key);
            basicCar.onKeyRelease(key, false);
        }
    }
    void HeightmapPhysicsRoom::keySpecial(int key, int x, int y) {
        if (topLevel.keyPress(key, true)) {
            switch(key) {
            case 1:
                uiScaleExp += 0.125f;
                uiScale = std::pow(2.0f, uiScaleExp);
                break;
            case 2:
                uiScaleExp -= 0.125f;
                uiScale = std::pow(2.0f, uiScaleExp);
                break;
            case 3:
                uiScaleExp = 0.0f;
                uiScale = std::pow(2.0f, uiScaleExp);
                break;
            default:
                if (!progressPopup.isOpen()) {
                    basicCar.onKeyPress(key, true);
                    camera.onKeyPressSpecial(key);
                }
                break;
            }
        }
    }
    void HeightmapPhysicsRoom::keySpecialRelease(int key, int x, int y) {
        if (topLevel.keyRelease(key, true) && !progressPopup.isOpen()) {
            camera.onKeyReleaseSpecial(key);
            basicCar.onKeyRelease(key, true);
        }
    }
    void HeightmapPhysicsRoom::mouseEvent(int button, int state, int x, int y) {
        if (topLevel.mouseEvent(button, state, x, y) && !progressPopup.isOpen()) {
            if (button == GLUT_LEFT_BUTTON) {
                if (state == GLUT_DOWN) {
                    // Pick up instances when we press left mouse.
                    grabInstance();
                } else if (state == GLUT_UP) {
                    // Put down any grabbed instance when we release left mouse.
                    releaseInstance();
                }
            } else if (button == GLUT_RIGHT_BUTTON) {
                if (state == GLUT_DOWN) {
                    // Start rotating instance when we press right mouse.
                    startRotatingInstance();
                } else if (state == GLUT_UP) {
                    // Put down any grabbed instance when we release right mouse.
                    stopRotatingInstance();
                }
            }
        }
    }
    void HeightmapPhysicsRoom::mouseMove(int x, int y) {
        if (topLevel.mouseMove(x, y) && !progressPopup.isOpen()) {
            camera.onMouseMove(x, y);
        }
    }
    void HeightmapPhysicsRoom::mouseDrag(int x, int y) {
        if (topLevel.mouseMove(x, y) && !progressPopup.isOpen()) {
            camera.onMouseMove(x, y);
        }
    }
    bool HeightmapPhysicsRoom::shouldDeleteOnRoomEnd(void) { return true; }

}
