/*
 * Medium quality (fast but poor quality parallax) variant of heightmap shader.
 * 
 * This variant runs (fairly) well, even on old intel integrated graphics.
 * To improve the performance of the parallax (at the expense of quality,
 * causing a few artifacts), we take these shortcuts:
 *  > Not enabling HIGH_QUALITY_MIPMAP means that mipmap derivatives are
 *    distorted along with the displaced texture. The alternative uses
 *    textureGrad to remove the distortion, but this causes terrible performance
 *    on older intel graphics.
 *  > Adding an offset to the ray starting position gives us tolerable results
 *    even when the sample count is reduced by one, (but does cause some
 *    artifacts near occluded areas - these artifacts are made less obvious
 *    though when HIGH_QUALITY_MIPMAP isn't enabled). Less samples means less
 *    texture lookups, 
 */

#define RAY_TRACE_SAMPLES 3
#define RAY_START_OFFSET 1.0f

#include <awe/heightmap/shader/fragment.glsl>
