/*
 * High quality (best looking) variant of heightmap shader.
 * 
 * For best quality, HIGH_QUALITY_MIPMAP can be enabled. This uses textureGrad
 * for consistent mipmap derivatives wherever textures are sampled. This
 * removes virtually all artifacts, and produces best quality - but does cause
 * major performance problems on older intel integrated graphics.
 */

#define HIGH_QUALITY_MIPMAP

#include <awe/heightmap/shader/fragment.glsl>
