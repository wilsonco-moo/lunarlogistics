/*
 * Low quality (fast) variant of heightmap shader.
 * 
 * For best performance, parallax can be completely disabled. This results
 * in flat looking terrain, but improves performance a lot by vastly simplifying
 * the shader.
 */

#define DISABLE_PARALLAX

#include <awe/heightmap/shader/fragment.glsl>
