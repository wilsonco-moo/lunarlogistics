/*
 * ProgressPopup.cpp
 *
 *  Created on: 29 Aug 2020
 *      Author: wilson
 */

#include "ProgressPopup.h"

#include <rascUI/util/Layout.h>
#include <rascUI/base/Theme.h>
#include <iostream>

#define LAYOUT_THEME theme

#define OVERALL_WIDTH  256
#define OVERALL_HEIGHT COUNT_OUTBORDER_Y(2)
#define OVERALL_LOC    GEN_CENTRE_FIXED(OVERALL_WIDTH, OVERALL_HEIGHT)

#define PROGRESSBAR_LOC \
    PIN_NORMAL_T(GEN_BORDER)

#define BOTTOM_PANEL_LOC                    \
    SUB_BORDERMARGIN_R(START_BUTTON_WIDTH,  \
     PIN_NORMAL_B(GEN_BORDER)               \
    )

#define START_BUTTON_WIDTH 64
#define START_BUTTON_LOC        \
    PIN_R(START_BUTTON_WIDTH,   \
     PIN_NORMAL_B(GEN_BORDER)   \
    )

namespace lunarlogistics {

    ProgressPopup::ProgressPopup(rascUI::Theme * theme) :
        rascUI::FloatingPanel(rascUI::Location(OVERALL_LOC)),
        
        progressControl(),
        progressBar(rascUI::Location(PROGRESSBAR_LOC)),
        bottomPanel(rascUI::Location(BOTTOM_PANEL_LOC)),
        bottomLabel(rascUI::Location(BOTTOM_PANEL_LOC), "Loading world..."),
        startButton(rascUI::Location(START_BUTTON_LOC), "Start",
            [this](GLfloat viewX, GLfloat viewY, int button) {
                if (button == getMb()) hide();
            }
        ) {
        
        contents.add(&progressBar);
        contents.add(&bottomPanel);
        contents.add(&bottomLabel);
        contents.add(&startButton);
        startButton.setActive(false);
    }
    
    ProgressPopup::~ProgressPopup(void) {
    }
    
    void ProgressPopup::updateRemaining(unsigned int remaining) {
        if (isOpen()) {
            float progressEstimate;
            bool hasChanged;
            progressControl.estimateProgress(&progressEstimate, &hasChanged, remaining);
            if (hasChanged) {
                std::cout << "Remaining: " << remaining << "\n";
                awe::ProgressControl::printProgress(progressEstimate, 40);
                progressBar.setProgress(progressEstimate);
                progressBar.repaint();
                if (remaining == 0) {
                    startButton.setActive(true);
                    bottomLabel.setText("Done.");
                }
            }
        }
    }
}
