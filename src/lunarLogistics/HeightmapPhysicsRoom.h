#ifndef NEWGL_LIGHTINGTESTROOM_H_
#define NEWGL_LIGHTINGTESTROOM_H_

#include <rascUItheme/themes/modernOpengl/modernBrightTheme/ModernBrightTheme.h>
#include <rascUI/components/toggleButton/ToggleButtonSeries.h>
#include <rascUI/components/toggleButton/ToggleButton.h>
#include <rascUI/components/generic/ProgressBar.h>
#include <rascUI/components/generic/FrontPanel.h>
#include <rascUI/components/generic/BackPanel.h>
#include <awe/heightmap/PhysicsTerrainScatter.h>
#include <awe/world/FirstPersonPhysicsCamera.h>
#include <rascUI/components/generic/Button.h>
#include <rascUI/components/generic/Label.h>
#include <awe/heightmap/PhysicsHeightMap.h>
#include <awe/world/InstanceFollowCamera.h>
#include <rascUI/base/TopLevelContainer.h>
#include <awe/util/physics/PhysicsWorld.h>
#include <awe/lighting/LightingManager.h>
#include <awe/heightmap/TerrainScatter.h>
#include <awe/texture/TextureManager.h>
#include <rascUI/util/EmplaceArray.h>
#include <wool/room/base/RoomBase.h>
#include <awe/audio/OpenALContext.h>
#include <awe/model/ModelManager.h>
#include <awe/asset/AssetManager.h>
#include <awe/world/WorldManager.h>
#include <awe/instance/Instance.h>
#include <threading/ThreadQueue.h>
#include <awe/world/FreeCamera.h>
#include <awe/util/types/Types.h>
#include <GL/gl.h>

#include "car/EngineSound.h"
#include "ProgressPopup.h"
#include "car/BasicCar.h"
#include "car/Gauge.h"

namespace lunarlogistics {

    // Uncomment to enable free camera mode.
    //#define FREE_CAMERA_MODE
    
    
    // Definition for shader quality options.
    #define SHADER_QUALITY_DEF \
        X(Low) X(Med) X(High)
    class ShaderQuality {
    public: 
        #define X(name) name,
        enum { SHADER_QUALITY_DEF COUNT };
        #undef X
        constexpr static int defaultQuality = Low;
    };
    

    class HeightmapPhysicsRoom : public wool::RoomBase {
    private:
        GLuint instanceShaderProgram, heightmapShaderProgram,
               instanceVpUniformLoc, heightmapVpUniformLoc, heightmapCamPosUniformLoc;
        threading::ThreadQueue threadQueue;
        
        // Awe and physics stuff.
        awe::OpenALContext openALContext;
        awe::PhysicsWorld physicsWorld;
        awe::TextureManager textureManager;
        awe::ModelManager modelManager;
        awe::AssetManager assetManager;
        awe::LightingManager lightingManager;
        //awe::FirstPersonPhysicsCamera camera;
        #ifdef FREE_CAMERA_MODE
            awe::FreeCamera camera;
        #else
            awe::InstanceFollowCamera camera;
        #endif
        awe::WorldManager worldManager;
        
        // Our heightmap.
        awe::PhysicsHeightMap heightmap;
        awe::TerrainScatter terrainScatter1, terrainScatter2;
        awe::PhysicsTerrainScatter physicsTerrainScatter;
        
        GLfloat timeStep;
        
        // Picking up objects (grabbing instances).
        awe::Instance * grabbedInstance;
        awe::GLvec3 positionDiff;
        GLfloat startHDir, startVDir;
        bool rotating;
        
        // Car
        BasicCar basicCar;
        EngineSound engineSound;
        Gauge gauge;
        
        // UI
        GLfloat viewWidth, viewHeight, uiScaleExp, uiScale;
        modernBright::ModernBrightTheme theme;
        rascUI::TopLevelContainer topLevel;
        rascUI::BackPanel qualityBackPanel;
        rascUI::ToggleButtonSeries qualitySeries;
        rascUI::EmplaceArray<rascUI::ToggleButton, ShaderQuality::COUNT> qualityButtons;
        rascUI::BackPanel displayBackPanel;
        rascUI::FrontPanel gearPanel;
        rascUI::Label gearLabel;
        rascUI::ProgressBar rpmBar;
        rascUI::Button resetButton, flipButton;
        rascUI::FrontPanel rpmPanel;
        rascUI::Label rpmLabel;
        rascUI::FrontPanel speedPanel;
        rascUI::Label speedLabel;
        ProgressPopup progressPopup;
        
    public:
        HeightmapPhysicsRoom(void);
        virtual ~HeightmapPhysicsRoom(void);
    
    private:
        
        void switchShader(int quality);
        
        // ------------- Instance grabbing --------------
        void grabInstance(void);
        void startRotatingInstance(void);
        void stopRotatingInstance(void);
        void releaseInstance(void);
        void updateGrabbing(void);
        // ----------------------------------------------
        
    public:
        virtual void init(void) override;
        virtual void step(void) override;
        virtual void end(void) override;
        virtual void keyNormal(unsigned char key, int x, int y) override;
        virtual void keyNormalRelease(unsigned char key, int x, int y) override;
        virtual void keySpecial(int key, int x, int y) override;
        virtual void keySpecialRelease(int key, int x, int y) override;
        virtual void reshape(GLsizei width, GLsizei height) override;
        virtual void mouseEvent(int button, int state, int x, int y) override;
        virtual void mouseMove(int x, int y) override;
        virtual void mouseDrag(int x, int y) override;
        virtual bool shouldDeleteOnRoomEnd(void) override;
    };
}

#endif
