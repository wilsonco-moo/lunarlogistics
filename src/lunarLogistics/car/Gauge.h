/*
 * Gauge.h
 *
 *  Created on: 27 Sep 2020
 *      Author: wilson
 */

#ifndef LUNARLOGISTICS_CAR_GAUGE_H_
#define LUNARLOGISTICS_CAR_GAUGE_H_

#include <rascUI/base/Component.h>
#include <vector>

namespace awe {
    class BufferVertexInfo;
}

namespace lunarlogistics {

    /**
     *
     */
    class Gauge : public rascUI::Component {
    private:
        std::vector<awe::BufferVertexInfo> vertexData;

    public:
        Gauge(rascUI::Location location);
        virtual ~Gauge(void);
        
    protected:
        virtual void onDraw(void) override;
        virtual bool shouldRespondToKeyboard(void) const override;
        virtual void recalculate(const rascUI::Rectangle & parentSize) override;
    };
}

#endif
