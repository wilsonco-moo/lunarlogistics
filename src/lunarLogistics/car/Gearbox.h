/*
 * Gearbox.h
 *
 *  Created on: 14 Sep 2020
 *      Author: wilson
 */

#ifndef LUNARLOGISTICS_CAR_GEARBOX_H_
#define LUNARLOGISTICS_CAR_GEARBOX_H_

#include <utility>
#include <string>
#include <vector>

namespace lunarlogistics {

    /**
     * GearBox stores a series of gear ratios, along with associated names.
     * 
     * Note: Gear ratios are specified as the speed multiplier, from engine
     *       to wheels. For example, a gear ratio which halves the engine speed
     *       (a low-ish gear) would have a ratio of 0.5. A gear ratio which
     *       doubles the engine speed (a very high overdrive gear) would have
     *       a ratio of 2. A gear ratio of zero is neutral.
     */
    class Gearbox {
    private:
        unsigned int gearCount,
                     currentGear;
        std::vector<float> ratios;
        std::vector<std::string> names;

    public:
        /**
         * This class is constructed with a sequence of gear names and ratios,
         * such as the following:
         * GearBox gears({{"Reverse", -0.1f}, {"Neutral", 0.0f},
         *                {"Gear 1",  0.1f},  {"Gear 2",  0.15f},
         *                {"Gear 3",  0.2f},  {"Gear 4",  0.3f}});
         */
        Gearbox(std::initializer_list<std::pair<std::string, float>> gears);
        
        // --------------------------- Get/set methods -------------------------
        
        /**
         * Gets the name of the specified gear.
         */
        inline const std::string & getGearName(unsigned int gear) const {
            return names[gear];
        }
        
        /**
         * Returns the total number of gears.
         */
        inline unsigned int getGearCount(void) const {
            return gearCount;
        }
        
        /**
         * Gets the currently selected gear.
         */
        inline unsigned int getCurrentGear(void) const {
            return currentGear;
        }
        
        /**
         * Sets the currently selected gear. Not that this must not exceed
         * the total number of gears (be greater than or equal to
         * getGearCount()).
         */
        inline void changeGear(unsigned int newCurrentGear) {
            currentGear = newCurrentGear;
        }
        
        /**
         * Adds the provided value to the current gear, (e.g: -1 will
         * shift down, +1 will shift up). Any amount can be specified, and
         * we won't shift below bottom gear, or above top gear.
         */
        void changeGearBy(int amount);
        
        /**
         * Returns whether the gearbox is currently in an engaged (i.e: non
         * neutral) gear. If this is true, torque/speed conversion methods are
         * not relevant (will always return zero values).
         */
        bool isEngaged(void) const;
        
        // ----------------------- Conversion methods --------------------------
        
        /**
         * These methods convert speed and torque from engine to wheels, and
         * vice versa.
         * If the current gear is neutral (zero gear ratio), all of these
         * methods will always return zero.
         */
        float engineToWheelSpeed(float engineSpeed) const;
        float engineToWheelTorque(float engineTorque) const;
        inline float wheelToEngineSpeed(float wheelSpeed) const { return engineToWheelTorque(wheelSpeed); }
        inline float wheelToEngineTorque(float wheelTorque) const { return engineToWheelSpeed(wheelTorque); }
    };
}

#endif
