/*
 * Modified from btRaycastVehicle.h, in bullet physics.
 * Daniel Wilson, September 2020.
 * Original copyright notice is below:
 * 
 * Copyright (c) 2005 Erwin Coumans http://continuousphysics.com/Bullet/
 *
 * Permission to use, copy, modify, distribute and sell this software
 * and its documentation for any purpose is hereby granted without fee,
 * provided that the above copyright notice appear in all copies.
 * Erwin Coumans makes no representations about the suitability 
 * of this software for any purpose.  
 * It is provided "as is" without express or implied warranty.
*/
#ifndef LUNARLOGISTICS_CAR_CUSTOMRAYCASTVEHICLE_H_
#define LUNARLOGISTICS_CAR_CUSTOMRAYCASTVEHICLE_H_

#include <bullet3/BulletDynamics/Dynamics/btRigidBody.h>
#include <bullet3/BulletDynamics/ConstraintSolver/btTypedConstraint.h>
#include <bullet3/LinearMath/btAlignedObjectArray.h>
#include <bullet3/BulletDynamics/Dynamics/btActionInterface.h>
#include <bullet3/LinearMath/btVector3.h>
#include <bullet3/LinearMath/btTransform.h>
#include <bullet3/BulletDynamics/Vehicle/btVehicleRaycaster.h>

//class btVehicleTuning;
class btRigidBody;
class btDynamicsWorld;

namespace lunarlogistics {

    // ============================= WHEEL INFO ================================

    struct CustomWheelInfoConstructionInfo
    {
        btVector3 m_chassisConnectionCS;
        btVector3 m_wheelDirectionCS;
        btVector3 m_wheelAxleCS;
        btScalar m_suspensionRestLength;
        btScalar m_maxSuspensionTravelCm;
        btScalar m_wheelRadius;
        btScalar m_mass;

        btScalar m_suspensionStiffness;
        btScalar m_wheelsDampingCompression;
        btScalar m_wheelsDampingRelaxation;
        btScalar m_frictionSlip;
        btScalar m_maxSuspensionForce;
        bool m_bIsFrontWheel;
    };

    /// CustomWheelInfo contains information per wheel about friction and suspension.
    struct CustomWheelInfo
    {
        struct RaycastInfo
        {
            //set by raycaster
            btVector3 m_contactNormalWS;  //contactnormal
            btVector3 m_contactPointWS;   //raycast hitpoint
            btScalar m_suspensionLength;
            btVector3 m_hardPointWS;       //raycast starting point
            btVector3 m_wheelDirectionWS;  //direction in worldspace
            btVector3 m_wheelAxleWS;       // axle in worldspace
            bool m_isInContact;
            void* m_groundObject;  //could be general void* ptr
        };

        RaycastInfo m_raycastInfo;

        btTransform m_worldTransform;

        btVector3 m_chassisConnectionPointCS;  //const
        btVector3 m_wheelDirectionCS;          //const
        btVector3 m_wheelAxleCS;               // const or modified by steering
        btScalar m_suspensionRestLength1;      //const
        btScalar m_maxSuspensionTravelCm;
        btScalar m_wheelsRadius;              //const
        btScalar m_suspensionStiffness;       //const
        btScalar m_wheelsDampingCompression;  //const
        btScalar m_wheelsDampingRelaxation;   //const
        btScalar m_frictionSlip;
        btScalar m_steering;
        btScalar m_rotation;
        btScalar m_rollInfluence;
        btScalar m_maxSuspensionForce;

        btScalar m_engineForce;

        btScalar m_brake;

        bool m_bIsFrontWheel;

        void* m_clientInfo;  //can be used to store pointer to sync transforms...

        btScalar m_clippedInvContactDotSuspension;
        btScalar m_suspensionRelativeVelocity;
        //calculated by suspension
        btScalar m_wheelsSuspensionForce;
        btScalar m_skidInfo;
        
        // Rotation, mass and angular velocity.
        btScalar m_mass;
        btScalar m_inertia; // Inertia about axis of rotation, calculated from mass.
        btScalar m_angularVelocity; // Angular velocity about axis of rotation.
        
        // Friction forces slowing down wheel.
        btScalar m_baseFrictionForce; // This force is always used to slow down wheel.
        btScalar m_axleFrictionForce; // This force is multiplied by the wheel's angular velocity.
        

        CustomWheelInfo() {}

        CustomWheelInfo(CustomWheelInfoConstructionInfo& ci)

        {
            m_suspensionRestLength1 = ci.m_suspensionRestLength;
            m_maxSuspensionTravelCm = ci.m_maxSuspensionTravelCm;

            m_wheelsRadius = ci.m_wheelRadius;
            m_suspensionStiffness = ci.m_suspensionStiffness;
            m_wheelsDampingCompression = ci.m_wheelsDampingCompression;
            m_wheelsDampingRelaxation = ci.m_wheelsDampingRelaxation;
            m_chassisConnectionPointCS = ci.m_chassisConnectionCS;
            m_wheelDirectionCS = ci.m_wheelDirectionCS;
            m_wheelAxleCS = ci.m_wheelAxleCS;
            m_frictionSlip = ci.m_frictionSlip;
            m_steering = btScalar(0.);
            m_engineForce = btScalar(0.);
            m_rotation = btScalar(0.);
            m_brake = btScalar(0.);
            m_rollInfluence = btScalar(0.1);
            m_bIsFrontWheel = ci.m_bIsFrontWheel;
            m_maxSuspensionForce = ci.m_maxSuspensionForce;
            
            m_mass = ci.m_mass;
            m_inertia = 0.5f * m_mass * m_wheelsRadius * m_wheelsRadius; // Inertia of cylinder is 0.5*m*r^2 (see https://en.wikipedia.org/wiki/List_of_moments_of_inertia).
            m_angularVelocity = 0.0f;
            
            m_baseFrictionForce = 20.0f;
            m_axleFrictionForce = 2.0f;
        }

        void updateWheel(const btRigidBody& chassis, RaycastInfo& raycastInfo);
        
        btScalar getSuspensionRestLength() const;
    };


    // =============================== RAYCAST VEHICLE =========================

    ///rayCast vehicle, very special constraint that turn a rigidbody into a vehicle.
    class CustomRaycastVehicle : public btActionInterface
    {
        btAlignedObjectArray<btVector3> m_forwardWS;
        btAlignedObjectArray<btVector3> m_axle;
        btAlignedObjectArray<btScalar> m_forwardImpulse;
        btAlignedObjectArray<btScalar> m_sideImpulse;

        ///backwards compatibility
        int m_userConstraintType;
        int m_userConstraintId;

    public:
        class btVehicleTuning
        {
        public:
            btVehicleTuning()
                : m_suspensionStiffness(btScalar(5.88)),
                  m_suspensionCompression(btScalar(0.83)),
                  m_suspensionDamping(btScalar(0.88)),
                  m_maxSuspensionTravelCm(btScalar(500.)),
                  m_frictionSlip(btScalar(10.5)),
                  m_maxSuspensionForce(btScalar(6000.))
            {
            }
            btScalar m_suspensionStiffness;
            btScalar m_suspensionCompression;
            btScalar m_suspensionDamping;
            btScalar m_maxSuspensionTravelCm;
            btScalar m_frictionSlip;
            btScalar m_maxSuspensionForce;
        };

    private:
        btVehicleRaycaster* m_vehicleRaycaster;
        btScalar m_pitchControl;
        btScalar m_steeringValue;
        btScalar m_currentVehicleSpeedKmHour;

        btRigidBody* m_chassisBody;

        int m_indexRightAxis;
        int m_indexUpAxis;
        int m_indexForwardAxis;

        void defaultInit(const btVehicleTuning& tuning);

    public:
        //constructor to create a car from an existing rigidbody
        CustomRaycastVehicle(const btVehicleTuning& tuning, btRigidBody* chassis, btVehicleRaycaster* raycaster);

        virtual ~CustomRaycastVehicle();

        ///btActionInterface interface
        virtual void updateAction(btCollisionWorld* collisionWorld, btScalar step)
        {
            (void)collisionWorld;
            updateVehicle(step);
        }
        
        // ======================== Static utility methods =====================
        
        /**
         * Inverts the reference to "value" if it is the opposite sign to "ref".
         */
        static void makeSameSign(btScalar ref, btScalar & value);
        
        /**
         * Inverts the reference to "value" if it is the same sign as "ref".
         */
        static void makeOppositeSign(btScalar ref, btScalar & value);
        
        /**
         * Subtracts "value" from "base". If doing so swapped the sign (went
         * across the zero barrier), or if base was zero to start with, then
         * base will end up as zero.
         */
        static void zeroBarrierSubtract(btScalar & base, btScalar value);
        
        /**
         * Converts "value" to be the same sign as "base", then performs a
         * zero round subtract of "value" from "base".
         */
        static void signlessZeroBarrierSubtract(btScalar & base, btScalar value);
        
        // =====================================================================

        ///btActionInterface interface
        void debugDraw(btIDebugDraw* debugDrawer);

        const btTransform& getChassisWorldTransform() const;

        btScalar rayCast(CustomWheelInfo& wheel);

        virtual void updateVehicle(btScalar step);

        void resetSuspension();

        btScalar getSteeringValue(int wheel) const;

        void setSteeringValue(btScalar steering, int wheel);
        
        btScalar getWheelAngularVelocity(int wheel) const;
        
        btScalar getWheelBaseFrictionForce(int wheel) const;
        
        btScalar getWheelAxleFrictionForce(int wheel) const;
        
        void setWheelBaseFrictionForce(btScalar value, int wheel);
        
        void setWheelAxleFrictionForce(btScalar value, int wheel);

        void applyEngineForce(btScalar force, int wheel);

        const btTransform& getWheelTransformWS(int wheelIndex) const;

        void updateWheelTransform(int wheelIndex, bool interpolatedTransform = true);

        //	void	setRaycastWheelInfo( int wheelIndex , bool isInContact, const btVector3& hitPoint, const btVector3& hitNormal,btScalar depth);

        CustomWheelInfo& addWheel(const btVector3& connectionPointCS0, const btVector3& wheelDirectionCS0, const btVector3& wheelAxleCS, btScalar suspensionRestLength, btScalar wheelRadius, btScalar wheelMass, const btVehicleTuning& tuning, bool isFrontWheel);

        inline int getNumWheels() const
        {
            return int(m_wheelInfo.size());
        }

        btAlignedObjectArray<CustomWheelInfo> m_wheelInfo;

        const CustomWheelInfo& getWheelInfo(int index) const;

        CustomWheelInfo& getWheelInfo(int index);

        void updateWheelTransformsWS(CustomWheelInfo& wheel, bool interpolatedTransform = true);

        void setBrake(btScalar brake, int wheelIndex);

        void setPitchControl(btScalar pitch)
        {
            m_pitchControl = pitch;
        }

        void updateSuspension(btScalar deltaTime);

        virtual void updateFriction(btScalar timeStep);

        inline btRigidBody* getRigidBody()
        {
            return m_chassisBody;
        }

        const btRigidBody* getRigidBody() const
        {
            return m_chassisBody;
        }

        inline int getRightAxis() const
        {
            return m_indexRightAxis;
        }
        inline int getUpAxis() const
        {
            return m_indexUpAxis;
        }

        inline int getForwardAxis() const
        {
            return m_indexForwardAxis;
        }

        ///Worldspace forward vector
        btVector3 getForwardVector() const
        {
            const btTransform& chassisTrans = getChassisWorldTransform();

            btVector3 forwardW(
                chassisTrans.getBasis()[0][m_indexForwardAxis],
                chassisTrans.getBasis()[1][m_indexForwardAxis],
                chassisTrans.getBasis()[2][m_indexForwardAxis]);

            return forwardW;
        }

        ///Velocity of vehicle (positive if velocity vector has same direction as foward vector)
        btScalar getCurrentSpeedKmHour() const
        {
            return m_currentVehicleSpeedKmHour;
        }

        virtual void setCoordinateSystem(int rightIndex, int upIndex, int forwardIndex)
        {
            m_indexRightAxis = rightIndex;
            m_indexUpAxis = upIndex;
            m_indexForwardAxis = forwardIndex;
        }

        ///backwards compatibility
        int getUserConstraintType() const
        {
            return m_userConstraintType;
        }

        void setUserConstraintType(int userConstraintType)
        {
            m_userConstraintType = userConstraintType;
        };

        void setUserConstraintId(int uid)
        {
            m_userConstraintId = uid;
        }

        int getUserConstraintId() const
        {
            return m_userConstraintId;
        }
    };
}

#endif  //BT_RAYCASTVEHICLE_H
