/*
 * Engine.cpp
 *
 *  Created on: 15 Sep 2020
 *      Author: wilson
 */

#include "Engine.h"

#include <algorithm>

#include "TorqueCurve.h"
#include "Gearbox.h"

namespace lunarlogistics {

    Engine::Engine(Gearbox * gearbox, TorqueCurve * torqueCurve, float idleSpeed, float rotationFriction, float inertia) :
        gearbox(gearbox),
        torqueCurve(torqueCurve),
        clutchEngaged(false),
        idleSpeed(idleSpeed),
        rotationSpeed(idleSpeed),
        throttle(0.0f),
        rotationFriction(rotationFriction),
        inertia(inertia) {
    }
    
    void Engine::updateEngine(float & engineTorque, float & engineFriction, float wheelSpeed, float timeStep) {
        
        // For engaged gears:
        if (gearbox->isEngaged()) {
            
            // Update rotation speed from wheels. Don't go below idle speed: pretend that the driver
            // disengages clutch at low speeds - for now, forcing the user to seperately control a clutch would be
            // too tedious, except for the 0.00001% of people who have foot pedals plugged into their computer.
            float speedFromWheels = gearbox->wheelToEngineSpeed(wheelSpeed);
            rotationSpeed = std::max(idleSpeed, speedFromWheels);
            clutchEngaged = (speedFromWheels >= idleSpeed);
            
            // Torque due to engine throttle, from torque curve. Note that only the positive component
            // of this has to be multiplied by throttle: RPM limiter should still act without throttle.
            float torqueCurveTorque = torqueCurve->getTorque(rotationSpeed),
                  throttleTorque = std::max(0.0f, torqueCurveTorque) * throttle +
                                   std::min(0.0f, torqueCurveTorque);
            
            // If going backwards, send engine friction to wheels only when there is throttle (player pushing
            // car forward with imaginary clutch). Otherwise, increase the engine friction to maximum as we approach
            // idle speed (where immaginary clutch becomes fully engaged).
            float frictionMultiplier;
            if (speedFromWheels < 0.0f) {
                frictionMultiplier = throttle;
            } else {
                frictionMultiplier = std::min(speedFromWheels / idleSpeed, 1.0f);
            }
            
            // Convert mechanical friction torque and torque from engine through gearbox.
            engineFriction = gearbox->engineToWheelTorque(rotationSpeed * rotationFriction * frictionMultiplier);
            engineTorque   = gearbox->engineToWheelTorque(throttleTorque);
            
        // When gearbox is in neutral:
        } else {
            
            // Add effects of throttle to engine rotational speed, then subtract effects of rotational friction.
            rotationSpeed += (torqueCurve->getTorque(rotationSpeed) * throttle * timeStep / inertia);
            rotationSpeed -= (rotationSpeed * rotationFriction * timeStep / inertia);
            
            // Make sure engine speed is not below idle.
            rotationSpeed = std::max(idleSpeed, rotationSpeed);
            
            // No torque and no friction, since gearbox is disengaged.
            engineTorque = 0.0f;
            engineFriction = 0.0f;
        }
    }
}
