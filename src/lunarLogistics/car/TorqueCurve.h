/*
 * TorqueCurve.h
 *
 *  Created on: 12 Sep 2020
 *      Author: wilson
 */

#ifndef LUNARLOGISTICS_CAR_TORQUECURVE_H_
#define LUNARLOGISTICS_CAR_TORQUECURVE_H_

#include <vector>

namespace lunarlogistics {

    /**
     * TorqueCurve aims to provide a simple model of the torque output of an
     * engine, depending on engine rotational speed.
     */
    class TorqueCurve {
    private:
        float minSpeed,
              maxSpeed;
        std::vector<float> values;
        float stepSize,
              startGradient,
              endGradient;
    public:
        /**
         * A minimum and maximum engine speed are required. A vector of torque
         * values specifies the curve, where the first matches up with minSpeed,
         * and the last matches up with maxSpeed. At least two torque values
         * are required.
         */
        TorqueCurve(float minSpeed, float maxSpeed, const std::vector<float> & values);
        
        /**
         * Returns the torque at the specified engine speed.
         * For engine speeds below/above the minimum/maximum speeds, engine
         * torque is extrapolated using the gradient between the first two
         * or last two torque values respectively.
         */
        float getTorque(float engineSpeed);
    };
}

#endif
