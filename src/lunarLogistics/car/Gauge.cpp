/*
 * Gauge.cpp
 *
 *  Created on: 27 Sep 2020
 *      Author: wilson
 */

#include "Gauge.h"

#include <rascUI/base/TopLevelContainer.h>
#include <awe/buffers/QuadBuffer.h>
#include <rascUI/base/Theme.h>

namespace lunarlogistics {

    Gauge::Gauge(rascUI::Location location) :
        rascUI::Component(location),
        vertexData() {
    }
    
    Gauge::~Gauge(void) {
    }
    
    void Gauge::onDraw(void) {
        rascUI::Theme * theme = getTopLevel()->theme;
        theme->drawFadePanel(location);
        
        //awe::QuadBuffer * buffer = (awe::QuadBuffer *)theme->getDrawBuffer();
        //buffer->vertexData(vertexData);
    }
    
    bool Gauge::shouldRespondToKeyboard(void) const {
        return false;
    }
    
    void Gauge::recalculate(const rascUI::Rectangle & parentSize) {
        Component::recalculate(parentSize);
        
        vertexData.clear();
        vertexData.push_back({awe::GLvec2(32, 32), {255, 0, 0, 255}});
        vertexData.push_back({awe::GLvec2(64, 32), {255, 0, 0, 255}});
        vertexData.push_back({awe::GLvec2(64, 64), {255, 0, 0, 255}});
        vertexData.push_back({awe::GLvec2(32, 64), {255, 0, 0, 255}});
        
        vertexData.shrink_to_fit();
    }
}
