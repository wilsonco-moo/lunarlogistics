/*
 * BasicCar.cpp
 *
 *  Created on: 25 Jul 2020
 *      Author: wilson
 */

#include "BasicCar.h"

#include <bullet3/LinearMath/btTransform.h>
#include <awe/util/types/ConversionUtil.h>
#include <awe/util/physics/PhysicsWorld.h>
#include <bullet3/LinearMath/btVector3.h>
#include <glm/gtc/matrix_transform.hpp>
#include <awe/heightmap/HeightMap.h>
#include <awe/world/WorldManager.h>
#include <awe/instance/Instance.h>
#include <glm/gtc/type_ptr.hpp>
#include <algorithm>
#include <iostream>
#include <cstddef>
#include <cmath>

#include "CustomRaycastVehicle.h"

#define CAR_ASSET       "assets/basicCar/basicCar.xml"
#define WHEEL_ASSET     "assets/basicCar/wheel.xml"
#define WHEEL_INV_ASSET "assets/basicCar/wheelInv.xml"

#define WHEEL_WIDTH 0.7f
#define WHEELBASE_F -1.48f
#define WHEELBASE_B 1.0f
#define WHEEL_ZOFF -0.22f
#define WHEEL_RADIUS 0.3f

#define WHEEL_DIRECTION btVector3(0, 0, -1)
#define AXLE_DIRECTION  btVector3(0, -1, 0)
#define SUSPENSION_REST_LEN 0.5f

#define RIGHT_INDEX 1
#define UP_INDEX 2
#define FORWARD_INDEX 0

#define FRONT_SUSPENSION_STIFFNESS (5.88f * 1.2f)
#define BACK_SUSPENSION_STIFFNESS  (9.0f * 1.2f)

#define ENGINE_FORCE -2000.0f
#define BRAKING_FORCE_F 720.0f
#define BRAKING_FORCE_B 7000.0f

#define MIN_STEERING -0.6f
#define MAX_STEERING 0.6f
#define STEERING_TURN_SPEED 1.0f

#define WHEEL_MASS 200.0f //50   A larger than realistic wheel mass helps to make wheel
                          //     rotation "feel" more realistic, especially when car is
                          //     overturned.

#define WHEEL_FRICTION_F 1.4f
#define WHEEL_FRICTION_B 1.0f

#define WHEEL_BASE_FRICTION_FORCE 8.0f  // Frictional force slowing down wheel
#define WHEEL_AXLE_FRICTION_FORCE 0.7f  // Frictional force slowing wheel down, multiplied by wheel speed.
#define ENGINE_FRICTION           0.07f // Friction multiplied by rotation of engine
#define ENGINE_INERTIA            0.08f // Inertia of engine (currently only modifies how the engine acts in neutral)

#define ENGINE_MAX_RPM 5400.0f  // Maximum RPM to display in UI system - should approximately line up with torque curve.


namespace lunarlogistics {
    
    // Wheels should first be transformed by this (to draw model correctly).
    const static awe::GLmat4 WHEEL_MODEL_TRANSFORM = glm::rotate(awe::GLmat4(1.0f), (GLfloat)M_PI, awe::GLvec3(0,0,1));

    // Specifies wheel IDs.
    class WheelIds {
    public:
        enum { frontLeft, frontRight, backLeft, backRight };
    };


    BasicCar::BasicCar(awe::AssetManager * assetManager, awe::PhysicsWorld * physicsWorld, awe::WorldManager * worldManager, const std::string & name) :
        btActionInterface(),
        
        name(name),
        initialPos(),
        loaded(false),
        assetManager(assetManager),
        physicsWorld(physicsWorld),
        worldManager(worldManager),
        
        wheelFL(assetManager, physicsWorld, WHEEL_ASSET,     name+"-wheelFL"),
        wheelFR(assetManager, physicsWorld, WHEEL_INV_ASSET, name+"-wheelFR"),
        wheelBL(assetManager, physicsWorld, WHEEL_ASSET,     name+"-wheelBL"),
        wheelBR(assetManager, physicsWorld, WHEEL_INV_ASSET, name+"-wheelBR"),
        
        car(NULL),
        raycaster(&physicsWorld->getDynamicsWorld()),
        raycastVehicle(NULL),
        
        accelerating(false),
        braking(false),
        steeringLeft(false),
        steeringRight(false),
        
        steeringAngle(0.0f),
        
        // See torque curve from following:
        // https://www.automobile-catalog.com/curve/1976/46310/lada_1200_l.html
        // All torque values in newton metres.
        // Note: The last torque value is made much lower, so the the gradient
        //       after it works like an RPM limiter.
        // Note: Original RPM range: 1000-6100. Adjusted for consistency with
        //       engine sounds.
        torqueCurve(
            700.0  * awe::ConversionUtil::RPM_TO_RADS, // minimum rpm of curve
            4800.0 * awe::ConversionUtil::RPM_TO_RADS, // maximum rpm of curve
            {23.1, 28.3, 33.3, 38.1, 42.6, 46.9, 51, 54.9, 58.6, 62, 65.2, 68.2,
             71, 73.6, 75.9, 78, 79.9, 81.6, 83, 84.2, 85.2, 86, 86.6, 86.9, 87,
             87, 86.9, 86.8, 86.6, 86.4, 86.1, 85.8, 85.4, 85, 84.5, 84, 83.4,
             82.8, 82.2, 81.4, 80.7, 79.9, 79, 78.1, 77.1, 76.1, 75, 73.4, 71.3,
             68.6, 65.5, /*62*/55.5}
        ),
        gearbox({{"Reverse", 0.038}, {"Neutral", 0.0f}, {"Gear 1", -0.04f},
                 {"Gear 2", -0.072f}, {"Gear 3", -0.1296f}, {"Gear 4", -0.23328f}}),
        engine(&gearbox, &torqueCurve, 1040 * awe::ConversionUtil::RPM_TO_RADS, ENGINE_FRICTION, ENGINE_INERTIA) {
        
        // Put gearbox in neutral by default.
        gearbox.changeGear(1);
    }
    
    BasicCar::~BasicCar(void) {
        if (raycastVehicle != NULL) {
            physicsWorld->getDynamicsWorld().removeAction(raycastVehicle);
            physicsWorld->getDynamicsWorld().removeAction(this);
            delete raycastVehicle;
        }
    }
    
    void BasicCar::initCar(void) {
        // Save the initial position of the car.
        initialPos = car->calculatePosition();
        
        // Make sure the car doesn't get deactivated.
        car->getRigidBody()->setActivationState(DISABLE_DEACTIVATION);
        
        raycastVehicle = new CustomRaycastVehicle(CustomRaycastVehicle::btVehicleTuning(), car->getRigidBody(), &raycaster);
        
        // Set the coordinate system, to stop the rotation going all wonky.
        raycastVehicle->setCoordinateSystem(RIGHT_INDEX, UP_INDEX, FORWARD_INDEX);
        
        // Add ourself and the vehicle to the dynamics world, using the action interface.
        physicsWorld->getDynamicsWorld().addAction(this);
        physicsWorld->getDynamicsWorld().addAction(raycastVehicle);
        
        {
            CustomRaycastVehicle::btVehicleTuning tuning;
            tuning.m_suspensionStiffness = FRONT_SUSPENSION_STIFFNESS;
            tuning.m_frictionSlip = WHEEL_FRICTION_F;
            
            // Front left
            raycastVehicle->addWheel(
                btVector3(WHEELBASE_F, -WHEEL_WIDTH, WHEEL_ZOFF), // Connection point
                WHEEL_DIRECTION, AXLE_DIRECTION, SUSPENSION_REST_LEN,
                WHEEL_RADIUS, WHEEL_MASS, tuning, true); // last true means is front wheel
                
            // Front right
            raycastVehicle->addWheel(
                btVector3(WHEELBASE_F, WHEEL_WIDTH, WHEEL_ZOFF), // Connection point
                WHEEL_DIRECTION, AXLE_DIRECTION, SUSPENSION_REST_LEN,
                WHEEL_RADIUS, WHEEL_MASS, tuning, true); // last true means is front wheel
        }
        
        {
            CustomRaycastVehicle::btVehicleTuning tuning;
            tuning.m_suspensionStiffness = BACK_SUSPENSION_STIFFNESS;
            tuning.m_frictionSlip = WHEEL_FRICTION_B;
            
            // Back left
            raycastVehicle->addWheel(
                btVector3(WHEELBASE_B, -WHEEL_WIDTH, WHEEL_ZOFF), // Connection point
                WHEEL_DIRECTION, AXLE_DIRECTION, SUSPENSION_REST_LEN,
                WHEEL_RADIUS, WHEEL_MASS, tuning, false); // last true means is front wheel
            
            // Back right
            raycastVehicle->addWheel(
                btVector3(WHEELBASE_B, WHEEL_WIDTH, WHEEL_ZOFF), // Connection point
                WHEEL_DIRECTION, AXLE_DIRECTION, SUSPENSION_REST_LEN,
                WHEEL_RADIUS, WHEEL_MASS, tuning, false); // last true means is front wheel
        }
        
        raycastVehicle->setWheelBaseFrictionForce(WHEEL_BASE_FRICTION_FORCE, WheelIds::frontLeft);
        raycastVehicle->setWheelBaseFrictionForce(WHEEL_BASE_FRICTION_FORCE, WheelIds::frontRight);
        raycastVehicle->setWheelBaseFrictionForce(WHEEL_BASE_FRICTION_FORCE, WheelIds::backLeft);
        raycastVehicle->setWheelBaseFrictionForce(WHEEL_BASE_FRICTION_FORCE, WheelIds::backRight);
        
        raycastVehicle->setWheelAxleFrictionForce(WHEEL_AXLE_FRICTION_FORCE, WheelIds::frontLeft);
        raycastVehicle->setWheelAxleFrictionForce(WHEEL_AXLE_FRICTION_FORCE, WheelIds::frontRight);
        raycastVehicle->setWheelAxleFrictionForce(WHEEL_AXLE_FRICTION_FORCE, WheelIds::backLeft);
        raycastVehicle->setWheelAxleFrictionForce(WHEEL_AXLE_FRICTION_FORCE, WheelIds::backRight);
    }
    
    void BasicCar::getWheelTransform(awe::GLmat4 & transform, int wheelIndex) {
        // First update the wheel transform, passing true to use interpolation.
        raycastVehicle->updateWheelTransform(wheelIndex, true);
        raycastVehicle->getWheelTransformWS(wheelIndex).getOpenGLMatrix(glm::value_ptr(transform));
        transform *= WHEEL_MODEL_TRANSFORM;
    }
    
    void BasicCar::updateAction(btCollisionWorld * collisionWorld, btScalar elapsed) {
        // Note that this is not actually called until we're loaded.
        
        // Update engine throttle.
        engine.setThrottle(accelerating ? 1.0f : 0.0f);
        
        // Work out current wheel rotational speed (average it across back wheels).
        float wheelSpeed = (raycastVehicle->getWheelAngularVelocity(WheelIds::backLeft) +
                            raycastVehicle->getWheelAngularVelocity(WheelIds::backRight)) * 0.5f;
        
        // Update engine torque from wheel speed.
        float engineTorque, engineFriction;
        engine.updateEngine(engineTorque, engineFriction, wheelSpeed, elapsed);
        
        // Halve engine torque since it is split across two wheels, do the same to the
        // engine friction, but also add WHEEL_BASE_FRICTION_FORCE. 
        engineTorque *= 0.5f;
        engineFriction = WHEEL_BASE_FRICTION_FORCE + engineFriction * 0.5f;
        
        // Set engine force and friction for both back wheels.
        raycastVehicle->applyEngineForce(engineTorque, WheelIds::backLeft);
        raycastVehicle->applyEngineForce(engineTorque, WheelIds::backRight);
        raycastVehicle->setWheelBaseFrictionForce(engineFriction, WheelIds::backLeft);
        raycastVehicle->setWheelBaseFrictionForce(engineFriction, WheelIds::backRight);
        
        // Update braking.
        if (braking) {
            raycastVehicle->setBrake(BRAKING_FORCE_F, WheelIds::frontLeft);
            raycastVehicle->setBrake(BRAKING_FORCE_F, WheelIds::frontRight);
            raycastVehicle->setBrake(BRAKING_FORCE_B, WheelIds::backLeft);
            raycastVehicle->setBrake(BRAKING_FORCE_B, WheelIds::backRight);
        } else {
            raycastVehicle->setBrake(0.0f, WheelIds::frontLeft);
            raycastVehicle->setBrake(0.0f, WheelIds::frontRight);
            raycastVehicle->setBrake(0.0f, WheelIds::backLeft);
            raycastVehicle->setBrake(0.0f, WheelIds::backRight);
        }
        
        // Update steering
        if (steeringLeft) {
            steeringAngle = std::min(MAX_STEERING, steeringAngle + elapsed * STEERING_TURN_SPEED);
        }
        if (steeringRight) {
            steeringAngle = std::max(MIN_STEERING, steeringAngle - elapsed * STEERING_TURN_SPEED);
        }
        if (!steeringLeft && !steeringRight && steeringAngle != 0.0f) {
            if (steeringAngle > 0) {
                steeringAngle -= elapsed * STEERING_TURN_SPEED;
                if (steeringAngle < 0) steeringAngle = 0.0f;
            } else {
                steeringAngle += elapsed * STEERING_TURN_SPEED;
                if (steeringAngle > 0) steeringAngle = 0.0f;
            }
        }
        raycastVehicle->setSteeringValue(steeringAngle, WheelIds::frontLeft);
        raycastVehicle->setSteeringValue(steeringAngle, WheelIds::frontRight);
    }
   
    void BasicCar::step(GLfloat elapsed /*, awe::HeightMap * heightMap*/) {
        
        // Initialise car if not found it yet.
        if (!loaded) {
            awe::Instance * newCar = worldManager->getInstance(CAR_ASSET, name);
            if (newCar == NULL || !newCar->isLoaded() || !newCar->getHasCollision()) {
                return;
            } else {
                car = newCar;
                initCar();
                loaded = true;
            }
        }
        
        /*
          else { // Uncomment this to "glue" the car to the terrain surface, for debugging purposes.
            if (heightMap->getIsLoaded()) {
                awe::GLvec3 pos = car->calculatePosition();
                GLfloat h = heightMap->getRealInterpHeight(pos.x, pos.y);
                car->setTransformation(glm::translate(awe::GLmat4(1.0f), awe::GLvec3(0.0f, 0.0f, h - pos.z)) * car->getTransformation());
                car->update();
            }
        }*/
        
        // Set wheel transforms. This must be done in step method instead of action
        // interface method, for the benefit of interpolation.
        awe::GLmat4 wheelTransform;
        
        getWheelTransform(wheelTransform, WheelIds::frontLeft);
        wheelFL.setTransformation(wheelTransform);
        wheelFL.update();
        
        getWheelTransform(wheelTransform, WheelIds::frontRight);
        wheelFR.setTransformation(wheelTransform);
        wheelFR.update();
        
        getWheelTransform(wheelTransform, WheelIds::backLeft);
        wheelBL.setTransformation(wheelTransform);
        wheelBL.update();
        
        getWheelTransform(wheelTransform, WheelIds::backRight);
        wheelBR.setTransformation(wheelTransform);
        wheelBR.update();
    }
    
    void BasicCar::onKeyPress(int key, bool special) {
        if (!special) {
            switch(key) {
            case 'w': case 'W':
                accelerating = true;
                break;
            case 'a': case 'A':
                steeringLeft = true;
                break;
            case 's': case 'S':
                braking = true;
                break;
            case 'd': case 'D':
                steeringRight = true;
                break;
            case 'q': case 'Q':
                gearbox.changeGearBy(-1);
                break;
            case 'e': case 'E':
                gearbox.changeGearBy(1);
                break;
            }
        }
    }
    
    void BasicCar::onKeyRelease(int key, bool special) {
        if (!special) {
            switch(key) {
            case 'w': case 'W':
                accelerating = false;
                break;
            case 'a': case 'A':
                steeringLeft = false;
                break;
            case 's': case 'S':
                braking = false;
                break;
            case 'd': case 'D':
                steeringRight = false;
                break;
            }
        }
    }
    
    GLfloat BasicCar::getSpeed(void) const {
        if (loaded) {
            return car->getRigidBody()->getLinearVelocity().length();
        } else {
            return 0.0f;
        }
    }
    
    void BasicCar::resetPosition(void) {
        if (loaded) {
            car->getRigidBody()->setCenterOfMassTransform(btTransform(
                btQuaternion::getIdentity(),
                awe::ConversionUtil::glmToBulletVec3(initialPos)
            ));
            car->getRigidBody()->setLinearVelocity(btVector3(0,0,0));
            car->getRigidBody()->setAngularVelocity(btVector3(0,0,0));
        }
    }
    
    void BasicCar::flip(void) {
        if (loaded) {
            car->getRigidBody()->setCenterOfMassTransform(btTransform(
                btQuaternion::getIdentity(),
                awe::ConversionUtil::glmToBulletVec3(car->calculatePosition() + awe::GLvec3(0,0,2))
            ));
            car->getRigidBody()->setLinearVelocity(btVector3(0,0,0));
            car->getRigidBody()->setAngularVelocity(btVector3(0,0,0));
        }
    }
    
    GLfloat BasicCar::getEngineRPM(void) const {
        return engine.getRotationSpeed() * awe::ConversionUtil::RADS_TO_RPM;
    }

    GLfloat BasicCar::getMaxEngineRPM(void) const {
        return ENGINE_MAX_RPM;
    }
    
    GLfloat BasicCar::getAverageDriveWheelSpeed(void) const {
        if (loaded) {
            return std::abs((raycastVehicle->getWheelAngularVelocity(WheelIds::backLeft) +
                             raycastVehicle->getWheelAngularVelocity(WheelIds::backRight)) * 0.5f * WHEEL_RADIUS);
        } else {
            return 0.0f;
        }
    }
    
    void BasicCar::debugDraw(btIDebugDraw * debugDrawer) {
    }
}
