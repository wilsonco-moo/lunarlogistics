/*
 * Engine.h
 *
 *  Created on: 15 Sep 2020
 *      Author: wilson
 */

#ifndef LUNARLOGISTICS_CAR_ENGINE_H_
#define LUNARLOGISTICS_CAR_ENGINE_H_

namespace lunarlogistics {
    class Gearbox;
    class TorqueCurve;

    /**
     * This class provides a simple simulation of a car's engine. Since
     * separately controlling a clutch is far too tedious for a computer game
     * (except for the 0.00001% of people who have foot pedals plugged into
     * their computer), for now this just assumes that the clutch is disengaged
     * whenever the wheels spin below the engine's idle speed.
     * 
     * Note: The engine has no maximum speed. However, it will stop producing
     *       positive torque once the rotation friction surpasses the torque
     *       from the torque curve. The torque curve can also be designed to
     *       output negative torque higher than a certain speed, to limit RPM.
     */
    class Engine {
    private:
        Gearbox * gearbox;
        TorqueCurve * torqueCurve;
        bool clutchEngaged;
        float idleSpeed,
              rotationSpeed,
              throttle,
              rotationFriction,
              inertia;
    public:
        /**
         * We require a gearbox and torque curve to specify the engine's properties.
         * Idle speed (in radians per second) is the minimum rotational speed,
         * which the engine will not rotate below.
         * Rotation friction is multiplied by the engine's rotational speed.
         * Inertia specifies how heavy the flywheel of the engine is.
         */
        Engine(Gearbox * gearbox, TorqueCurve * torqueCurve, float idleSpeed, float rotationFriction, float inertia);
        
        // ------------------------ Get/set methods ----------------------------
        
        /**
         * Allows access to our gearbox.
         */
        inline Gearbox * getGearbox(void) const {
            return gearbox;
        }
        
        /**
         * Allows access to our torque curve.
         */
        inline TorqueCurve * getTorqueCurve(void) const {
            return torqueCurve;
        }
        
        /**
         * Sets the engine's throttle. This should be a value between zero and
         * one.
         */
        inline void setThrottle(float newThrottle) {
            throttle = newThrottle;
        }
        
        /**
         * Gets the current engine throttle.
         */
        inline float getThrottle(void) const {
            return throttle;
        }
        
        /**
         * Gets the engine's current rotational speed (radians per second).
         */
        inline float getRotationSpeed(void) const {
            return rotationSpeed;
        }
        
        /**
         * Returns true if the clutch is *fully* engaged - i.e: the engine is
         * completely locked to the wheels. This happens only when the wheels
         * are spinning at a speed which allows the engine to spin above
         * its idle speed.
         */
        inline bool isClutchFullyEngaged(void) const {
            return clutchEngaged;
        }
        
        // --------------------------- Engine update ---------------------------
        
        /**
         * This should be run each physics tick or frame. This outputs the
         * current total engine torque and friction, provided with the current
         * wheel speed and time step.
         */
        void updateEngine(float & engineTorque, float & engineFriction, float wheelSpeed, float timeStep);
    };
}

#endif
