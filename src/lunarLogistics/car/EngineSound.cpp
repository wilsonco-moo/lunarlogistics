/*
 * EngineSound.cpp
 *
 *  Created on: 20 Sep 2020
 *      Author: wilson
 */

#include "EngineSound.h"

#include <awe/util/types/ConversionUtil.h>
#include <awe/audio/OpenALContext.h>
#include <threading/ThreadQueue.h>
#include <tinyxml2/tinyxml2.h>
#include <awe/util/Util.h>
#include <algorithm>
#include <iostream>
#include <cmath>

namespace lunarlogistics {
    
    // -------------------- Internal Sound class -----------------------
    
    EngineSound::Sound::Sound(const char * filename, float engineSpeed, size_t soundLength) :
        buffer(0),
        source(0),
        engineSpeed(engineSpeed),
        data(new int16_t[soundLength]),
        playing(false) {
        awe::OpenALContext::loadSoundData(data, soundLength, filename);
    }
    
    EngineSound::Sound::~Sound(void) {
        delete[] data;
        if (source != 0) alDeleteSources(1, &source);
        if (buffer != 0) alDeleteBuffers(1, &buffer);
        awe::OpenALContext::hasError();
    }
    
    bool EngineSound::Sound::loadResources(size_t soundLength, size_t sampleRate) {
        alGenBuffers(1, &buffer);
        alBufferData(buffer, AL_FORMAT_MONO16, data, soundLength * sizeof(int16_t), sampleRate);
        alGenSources(1, &source);
        alSourcei(source, AL_BUFFER, buffer);
        alSourcei(source, AL_LOOPING, AL_TRUE);
        delete[] data; // Data is no longer needed now it is in OpenAL buffer.
        data = NULL;
        return !awe::OpenALContext::hasError();
    }
    
    void EngineSound::Sound::playEngineSpeed(float realEngineSpeed, float gain) {
        alSourcef(source, AL_PITCH, realEngineSpeed / engineSpeed);
        alSourcef(source, AL_GAIN, gain);
        if (!playing) {
            alSourcePlay(source);
            playing = true;
        }
        awe::OpenALContext::hasError();
    }

    void EngineSound::Sound::pause(void) {
        if (playing) {
            alSourcePause(source);
            awe::OpenALContext::hasError();
            playing = false;
        }
    }
    
    // -----------------------------------------------------------------
    
    EngineSound::EngineSound(threading::ThreadQueue * threadQueue, const std::string & soundDirectory, float volumeMultiplier) :
        volumeMultiplier(volumeMultiplier),
        threadQueue(threadQueue),
        token(threadQueue->createToken()),
        loaded(false),
        sounds() {
        
        loadJob = threadQueue->add(token, [this, dir = std::string(soundDirectory)](void) {
            load(dir);
        });
    }
    
    EngineSound::~EngineSound(void) {
        threadQueue->deleteToken(token);
    }
    
    void EngineSound::setupFailed(void) {
        // Default values for when loading XML file fails.
        sampleRate = 8000;
        soundLength = 4000;
        minSpeed = 0.0f;
        maxSpeed = 1000.0f;
        sounds.resize(0);
    }
    
    // When any XML errors happen, print the error and filename, setup failed, and return.
    #define PRINT_XML_ERROR(xmlErr)                                              \
        std::cerr << "WARNING: EngineSound: Failed to load: " << xmlErr << '\n'; \
        setupFailed();                                                           \
        return;
    
    void EngineSound::load(const std::string & soundDirectory) {
        // Read document, get root element.
        tinyxml2::XMLDocument document;
        std::string xmlFile = soundDirectory + "/engineSound.xml";
        if (document.LoadFile(xmlFile.c_str()) != tinyxml2::XML_SUCCESS) {
            PRINT_XML_ERROR("Failed to load XML file: " << xmlFile)
        }
        GET_ELEM(engineSoundElem, &document, "engineSound")
        
        // Read properties.
        GET_ELEM(propertiesElem, engineSoundElem, "properties")
        GET_ATTRIB_UINT(sampleRateAttr, propertiesElem, "sampleRate")
        GET_ATTRIB_UINT(lengthAttr, propertiesElem, "length")
        sampleRate = sampleRateAttr;
        soundLength = lengthAttr;
        
        // Sound length must be greater than zero.
        if (soundLength < 1) {
            PRINT_XML_ERROR("Sound length (in samples) must be greater than zero.")
        }
        
        // Count sounds, resize sounds EmplaceVector.
        size_t soundCount = 0;
        GET_ELEM(soundsElem, engineSoundElem, "sounds")
        ELEM_CHILDREN_FOR_EACH(soundElem, soundsElem, "sound") {
            soundCount++;
        }
        sounds.resize(soundCount);
        
        // We can't have fewer than two sounds.
        if (soundCount < 2) {
            PRINT_XML_ERROR("At least two engine sounds are required.")
        }
        
        minSpeed = 0.0f;
        maxSpeed = 0.0f;
        
        // Create sound instances from XML data.
        bool first = true;
        ELEM_CHILDREN_FOR_EACH(soundElem, soundsElem, "sound") {
            
            // Read filename and engine speed, convert rpm to rads/s.
            GET_ATTRIB(filename, soundElem, "filename")
            GET_ATTRIB_FLOAT(rpmAttr, soundElem, "rpm")
            float speed = rpmAttr * awe::ConversionUtil::RPM_TO_RADS;
            
            // Record min speed (first speed value) and max speed. Complain and give up if speed is not greater than previous.
            if (first) {
                first = false;
                minSpeed = speed;
            }
            if (speed <= maxSpeed) {
                PRINT_XML_ERROR("The RPM value for each engine speed sound must be larger than the previous one.")
            }
            maxSpeed = speed;
            
            sounds.emplace(filename, speed, soundLength);
        }
    }
    
    void EngineSound::onLoaded(void) {
        // Load resources for all engine sounds. If doing so fails, setup failed and give up.
        for (Sound & sound : sounds) {
            if (!sound.loadResources(soundLength, sampleRate)) {
                std::cerr << "WARNING: EngineSound: " << "Failed to load OpenAL resources.\n";
                setupFailed();
                return;
            }
        }
        if (!sounds.empty()) std::cout << "Successfully completed setup of engine sound.\n";
    }

    void EngineSound::updateEngineSpeed(float engineSpeed) {
        // Do nothing if not loaded.
        if (!loaded) {
            if (threadQueue->getJobStatus(loadJob) == threading::JobStatus::complete) {
                loaded = true;
                onLoaded();
            } else {
                return;
            }
        }
        
        // Do nothing if we loaded no sounds (loading failed).
        if (sounds.empty()) {
            return;
        }
        
        // Make sure engine speed isn't negative.
        engineSpeed = std::max(0.0f, engineSpeed);
        engineSpeed *= 0.75;
        
        // If engine speed is outside min/max range, play first or last sound and pause all others.
        if (engineSpeed <= minSpeed) {
            sounds[0].playEngineSpeed(engineSpeed, volumeMultiplier);
            for (unsigned int i = 1; i < sounds.size(); i++) { sounds[i].pause(); }
            return;
        } else if (engineSpeed >= maxSpeed) {
            sounds[sounds.size() - 1].playEngineSpeed(engineSpeed, volumeMultiplier);
            for (unsigned int i = 0; i < sounds.size() - 1; i++) { sounds[i].pause(); }
            return;
        }
        
        // Find index of first sound with greater engine speed.
        unsigned int upperSound = 1;
        for (unsigned int i = 1; i < sounds.size(); i++) {
            if (sounds[i].getEngineSpeed() > engineSpeed) {
                upperSound = i;
                break;
            }
        }
        
        // Pause all sounds except upperSound and the one before it.
        for (unsigned int i = 0; i < sounds.size(); i++) {
            if (i != upperSound && i != upperSound - 1) {
                sounds[i].pause();
            }
        }
        
        // Figure out volumes to play both engine sounds.
        float lowerEngineSpeed = sounds[upperSound - 1].getEngineSpeed(),
              upperEngineSpeed = sounds[upperSound].getEngineSpeed(),
                           mix = (engineSpeed - lowerEngineSpeed) / (upperEngineSpeed - lowerEngineSpeed),
                     lowerGain = std::sqrt(0.5 + 0.5 * std::cos(M_PI * mix)) * volumeMultiplier, // Equal power curve, see https://stackoverflow.com/a/11280084
                     upperGain = std::sqrt(0.5 - 0.5 * std::cos(M_PI * mix)) * volumeMultiplier;
        
        // Play both engine sounds.
        sounds[upperSound - 1].playEngineSpeed(engineSpeed, lowerGain);
        sounds[upperSound].playEngineSpeed(engineSpeed, upperGain);
    }
}
