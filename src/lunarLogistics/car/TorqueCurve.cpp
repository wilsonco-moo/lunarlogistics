/*
 * TorqueCurve.cpp
 *
 *  Created on: 12 Sep 2020
 *      Author: wilson
 */

#include "TorqueCurve.h"

#include <glm/common.hpp>
#include <iostream>
#include <cstdlib>
#include <cmath>

namespace lunarlogistics {

    TorqueCurve::TorqueCurve(float minSpeed, float maxSpeed, const std::vector<float> & values) :
        minSpeed(minSpeed),
        maxSpeed(maxSpeed),
        values(values),
        stepSize((maxSpeed - minSpeed) / (values.size() - 1)) {
        
        if (values.size() < 2) {
            std::cerr << "ERROR: TorqueCurve: At least two speed values are required.\n";
            exit(EXIT_FAILURE);
        }
        
        startGradient = (values[1] - values[0]) / stepSize;
        endGradient = (values[values.size() - 1] - values[values.size() - 2]) / stepSize;
    }
    
    float TorqueCurve::getTorque(float engineSpeed) {
        // Scale engine speed, convert to integer position and offset.
        float scaledSpeed = (engineSpeed - minSpeed) / stepSize;
        int position = std::floor(scaledSpeed);
        float offset = scaledSpeed - position;
        
        // Use gradients for values above and below range. Compare using integer
        // position to avoid inconsistencies from rounding.
        if (position < 0) {
            return values[0] - (minSpeed - engineSpeed) * startGradient;
        } else if (position >= (int)(values.size() - 1)) {
            return values[values.size() - 1] + (engineSpeed - maxSpeed) * endGradient;
        
        // Interpolate two values otherwise.
        } else {
            return glm::mix(values[position], values[position + 1], offset);
        }
    }
}
