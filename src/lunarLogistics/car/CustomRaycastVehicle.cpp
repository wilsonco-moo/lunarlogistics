/*
 * Modified from btRaycastVehicle.cpp, in bullet physics.
 * Daniel Wilson, September 2020.
 * Original copyright notice is below:
 * 
 * Copyright (c) 2005 Erwin Coumans http://continuousphysics.com/Bullet/
 *
 * Permission to use, copy, modify, distribute and sell this software
 * and its documentation for any purpose is hereby granted without fee,
 * provided that the above copyright notice appear in all copies.
 * Erwin Coumans makes no representations about the suitability 
 * of this software for any purpose.  
 * It is provided "as is" without express or implied warranty.
*/

#include "CustomRaycastVehicle.h"

#include <bullet3/LinearMath/btVector3.h>
#include <bullet3/BulletDynamics/ConstraintSolver/btSolve2LinearConstraint.h>
#include <bullet3/BulletDynamics/ConstraintSolver/btJacobianEntry.h>
#include <bullet3/LinearMath/btQuaternion.h>
#include <bullet3/BulletDynamics/Dynamics/btDynamicsWorld.h>
#include <bullet3/BulletDynamics/Vehicle/btVehicleRaycaster.h>
#include <bullet3/LinearMath/btMinMax.h>
#include <bullet3/LinearMath/btIDebugDraw.h>
#include <bullet3/BulletDynamics/ConstraintSolver/btContactConstraint.h>

#include <algorithm>
#include <iostream>
#include <cmath>

#define ROLLING_INFLUENCE_FIX

namespace lunarlogistics {

    // ============================ WHEEL INFO =================================
    
    btScalar CustomWheelInfo::getSuspensionRestLength() const
    {
        return m_suspensionRestLength1;
    }

    
    // ============================ RAYCAST VEHICLE ============================

    /*
    // This is defined in btRaycastVehicle.cpp already, so does not need redefining.
    btRigidBody& btActionInterface::getFixedBody()
    {
        static btRigidBody s_fixed(0, 0, 0);
        s_fixed.setMassProps(btScalar(0.), btVector3(btScalar(0.), btScalar(0.), btScalar(0.)));
        return s_fixed;
    }
    */

    CustomRaycastVehicle::CustomRaycastVehicle(const btVehicleTuning& tuning, btRigidBody* chassis, btVehicleRaycaster* raycaster)
        : m_vehicleRaycaster(raycaster),
          m_pitchControl(btScalar(0.))
    {
        m_chassisBody = chassis;
        m_indexRightAxis = 0;
        m_indexUpAxis = 2;
        m_indexForwardAxis = 1;
        defaultInit(tuning);
    }

    void CustomRaycastVehicle::defaultInit(const btVehicleTuning& tuning)
    {
        (void)tuning;
        m_currentVehicleSpeedKmHour = btScalar(0.);
        m_steeringValue = btScalar(0.);
    }

    CustomRaycastVehicle::~CustomRaycastVehicle()
    {
    }
    
    void CustomRaycastVehicle::makeSameSign(btScalar ref, btScalar & value) {
        if ((ref < 0) != (value < 0)) {
            value *= -1.0f;
        }
    }
     
    void CustomRaycastVehicle::makeOppositeSign(btScalar ref, btScalar & value) {
        if ((ref < 0) == (value < 0)) {
            value *= -1.0f;
        }
    }
    
    void CustomRaycastVehicle::zeroBarrierSubtract(btScalar & base, btScalar value) {
        if (base == 0.0f) {
            return;
        }
        btScalar oldBase = base,
                 newBase = base - value;
        if ((oldBase < 0) == (newBase < 0)) {
            base = newBase;
        } else {
            base = 0.0f;
        }
    }
    
    void CustomRaycastVehicle::signlessZeroBarrierSubtract(btScalar & base, btScalar value) {
        makeSameSign(base, value);
        zeroBarrierSubtract(base, value);
    }

    //
    // basically most of the code is general for 2 or 4 wheel vehicles, but some of it needs to be reviewed
    //
    CustomWheelInfo& CustomRaycastVehicle::addWheel(const btVector3& connectionPointCS, const btVector3& wheelDirectionCS0, const btVector3& wheelAxleCS, btScalar suspensionRestLength, btScalar wheelRadius, btScalar wheelMass, const btVehicleTuning& tuning, bool isFrontWheel)
    {
        CustomWheelInfoConstructionInfo ci;

        ci.m_chassisConnectionCS = connectionPointCS;
        ci.m_wheelDirectionCS = wheelDirectionCS0;
        ci.m_wheelAxleCS = wheelAxleCS;
        ci.m_suspensionRestLength = suspensionRestLength;
        ci.m_wheelRadius = wheelRadius;
        ci.m_suspensionStiffness = tuning.m_suspensionStiffness;
        ci.m_wheelsDampingCompression = tuning.m_suspensionCompression;
        ci.m_wheelsDampingRelaxation = tuning.m_suspensionDamping;
        ci.m_frictionSlip = tuning.m_frictionSlip;
        ci.m_bIsFrontWheel = isFrontWheel;
        ci.m_maxSuspensionTravelCm = tuning.m_maxSuspensionTravelCm;
        ci.m_maxSuspensionForce = tuning.m_maxSuspensionForce;
        ci.m_mass = wheelMass;

        m_wheelInfo.push_back(CustomWheelInfo(ci));

        CustomWheelInfo& wheel = m_wheelInfo[getNumWheels() - 1];

        updateWheelTransformsWS(wheel, false);
        updateWheelTransform(getNumWheels() - 1, false);
        return wheel;
    }

    const btTransform& CustomRaycastVehicle::getWheelTransformWS(int wheelIndex) const
    {
        btAssert(wheelIndex < getNumWheels());
        const CustomWheelInfo& wheel = m_wheelInfo[wheelIndex];
        return wheel.m_worldTransform;
    }

    void CustomRaycastVehicle::updateWheelTransform(int wheelIndex, bool interpolatedTransform)
    {
        CustomWheelInfo& wheel = m_wheelInfo[wheelIndex];
        updateWheelTransformsWS(wheel, interpolatedTransform);
        btVector3 up = -wheel.m_raycastInfo.m_wheelDirectionWS;
        const btVector3& right = wheel.m_raycastInfo.m_wheelAxleWS;
        btVector3 fwd = up.cross(right);
        fwd = fwd.normalize();
        //	up = right.cross(fwd);
        //	up.normalize();

        //rotate around steering over de wheelAxleWS
        btScalar steering = wheel.m_steering;

        btQuaternion steeringOrn(up, steering);  //wheel.m_steering);
        btMatrix3x3 steeringMat(steeringOrn);

        btQuaternion rotatingOrn(right, -wheel.m_rotation);
        btMatrix3x3 rotatingMat(rotatingOrn);

        btMatrix3x3 basis2;
        basis2[0][m_indexRightAxis] = -right[0];
        basis2[1][m_indexRightAxis] = -right[1];
        basis2[2][m_indexRightAxis] = -right[2];

        basis2[0][m_indexUpAxis] = up[0];
        basis2[1][m_indexUpAxis] = up[1];
        basis2[2][m_indexUpAxis] = up[2];

        basis2[0][m_indexForwardAxis] = fwd[0];
        basis2[1][m_indexForwardAxis] = fwd[1];
        basis2[2][m_indexForwardAxis] = fwd[2];

        wheel.m_worldTransform.setBasis(steeringMat * rotatingMat * basis2);
        wheel.m_worldTransform.setOrigin(
            wheel.m_raycastInfo.m_hardPointWS + wheel.m_raycastInfo.m_wheelDirectionWS * wheel.m_raycastInfo.m_suspensionLength);
    }

    void CustomRaycastVehicle::resetSuspension()
    {
        int i;
        for (i = 0; i < m_wheelInfo.size(); i++)
        {
            CustomWheelInfo& wheel = m_wheelInfo[i];
            wheel.m_raycastInfo.m_suspensionLength = wheel.getSuspensionRestLength();
            wheel.m_suspensionRelativeVelocity = btScalar(0.0);

            wheel.m_raycastInfo.m_contactNormalWS = -wheel.m_raycastInfo.m_wheelDirectionWS;
            //wheel_info.setContactFriction(btScalar(0.0));
            wheel.m_clippedInvContactDotSuspension = btScalar(1.0);
        }
    }

    void CustomRaycastVehicle::updateWheelTransformsWS(CustomWheelInfo& wheel, bool interpolatedTransform)
    {
        wheel.m_raycastInfo.m_isInContact = false;

        btTransform chassisTrans = getChassisWorldTransform();
        if (interpolatedTransform && (getRigidBody()->getMotionState()))
        {
            getRigidBody()->getMotionState()->getWorldTransform(chassisTrans);
        }

        wheel.m_raycastInfo.m_hardPointWS = chassisTrans(wheel.m_chassisConnectionPointCS);
        wheel.m_raycastInfo.m_wheelDirectionWS = chassisTrans.getBasis() * wheel.m_wheelDirectionCS;
        wheel.m_raycastInfo.m_wheelAxleWS = chassisTrans.getBasis() * wheel.m_wheelAxleCS;
    }

    btScalar CustomRaycastVehicle::rayCast(CustomWheelInfo& wheel)
    {
        updateWheelTransformsWS(wheel, false);

        btScalar depth = -1;

        btScalar raylen = wheel.getSuspensionRestLength() + wheel.m_wheelsRadius;

        btVector3 rayvector = wheel.m_raycastInfo.m_wheelDirectionWS * (raylen);
        const btVector3& source = wheel.m_raycastInfo.m_hardPointWS;
        wheel.m_raycastInfo.m_contactPointWS = source + rayvector;
        const btVector3& target = wheel.m_raycastInfo.m_contactPointWS;

        btScalar param = btScalar(0.);

        btVehicleRaycaster::btVehicleRaycasterResult rayResults;

        btAssert(m_vehicleRaycaster);

        void* object = m_vehicleRaycaster->castRay(source, target, rayResults);

        wheel.m_raycastInfo.m_groundObject = 0;

        if (object)
        {
            param = rayResults.m_distFraction;
            depth = raylen * rayResults.m_distFraction;
            wheel.m_raycastInfo.m_contactNormalWS = rayResults.m_hitNormalInWorld;
            wheel.m_raycastInfo.m_isInContact = true;

            wheel.m_raycastInfo.m_groundObject = &getFixedBody();  ///@todo for driving on dynamic/movable objects!;
            //wheel.m_raycastInfo.m_groundObject = object;

            btScalar hitDistance = param * raylen;
            wheel.m_raycastInfo.m_suspensionLength = hitDistance - wheel.m_wheelsRadius;
            //clamp on max suspension travel

            btScalar minSuspensionLength = wheel.getSuspensionRestLength() - wheel.m_maxSuspensionTravelCm * btScalar(0.01);
            btScalar maxSuspensionLength = wheel.getSuspensionRestLength() + wheel.m_maxSuspensionTravelCm * btScalar(0.01);
            if (wheel.m_raycastInfo.m_suspensionLength < minSuspensionLength)
            {
                wheel.m_raycastInfo.m_suspensionLength = minSuspensionLength;
            }
            if (wheel.m_raycastInfo.m_suspensionLength > maxSuspensionLength)
            {
                wheel.m_raycastInfo.m_suspensionLength = maxSuspensionLength;
            }

            wheel.m_raycastInfo.m_contactPointWS = rayResults.m_hitPointInWorld;

            btScalar denominator = wheel.m_raycastInfo.m_contactNormalWS.dot(wheel.m_raycastInfo.m_wheelDirectionWS);

            btVector3 chassis_velocity_at_contactPoint;
            btVector3 relpos = wheel.m_raycastInfo.m_contactPointWS - getRigidBody()->getCenterOfMassPosition();

            chassis_velocity_at_contactPoint = getRigidBody()->getVelocityInLocalPoint(relpos);

            btScalar projVel = wheel.m_raycastInfo.m_contactNormalWS.dot(chassis_velocity_at_contactPoint);

            if (denominator >= btScalar(-0.1))
            {
                wheel.m_suspensionRelativeVelocity = btScalar(0.0);
                wheel.m_clippedInvContactDotSuspension = btScalar(1.0) / btScalar(0.1);
            }
            else
            {
                btScalar inv = btScalar(-1.) / denominator;
                wheel.m_suspensionRelativeVelocity = projVel * inv;
                wheel.m_clippedInvContactDotSuspension = inv;
            }
        }
        else
        {
            //put wheel info as in rest position
            wheel.m_raycastInfo.m_suspensionLength = wheel.getSuspensionRestLength();
            wheel.m_suspensionRelativeVelocity = btScalar(0.0);
            wheel.m_raycastInfo.m_contactNormalWS = -wheel.m_raycastInfo.m_wheelDirectionWS;
            wheel.m_clippedInvContactDotSuspension = btScalar(1.0);
        }

        return depth;
    }

    const btTransform& CustomRaycastVehicle::getChassisWorldTransform() const
    {
        /*if (getRigidBody()->getMotionState())
        {
            btTransform chassisWorldTrans;
            getRigidBody()->getMotionState()->getWorldTransform(chassisWorldTrans);
            return chassisWorldTrans;
        }
        */

        return getRigidBody()->getCenterOfMassTransform();
    }

    void CustomRaycastVehicle::updateVehicle(btScalar step)
    {
        {
            for (int i = 0; i < getNumWheels(); i++)
            {
                updateWheelTransform(i, false);
            }
        }

        m_currentVehicleSpeedKmHour = btScalar(3.6) * getRigidBody()->getLinearVelocity().length();

        const btTransform& chassisTrans = getChassisWorldTransform();

        btVector3 forwardW(
            chassisTrans.getBasis()[0][m_indexForwardAxis],
            chassisTrans.getBasis()[1][m_indexForwardAxis],
            chassisTrans.getBasis()[2][m_indexForwardAxis]);

        if (forwardW.dot(getRigidBody()->getLinearVelocity()) < btScalar(0.))
        {
            m_currentVehicleSpeedKmHour *= btScalar(-1.);
        }

        //
        // simulate suspension
        //

        int i = 0;
        for (i = 0; i < m_wheelInfo.size(); i++)
        {
            //btScalar depth;
            //depth =
            rayCast(m_wheelInfo[i]);
        }

        updateSuspension(step);

        for (i = 0; i < m_wheelInfo.size(); i++)
        {
            //apply suspension force
            CustomWheelInfo& wheel = m_wheelInfo[i];

            btScalar suspensionForce = wheel.m_wheelsSuspensionForce;

            if (suspensionForce > wheel.m_maxSuspensionForce)
            {
                suspensionForce = wheel.m_maxSuspensionForce;
            }
            btVector3 impulse = wheel.m_raycastInfo.m_contactNormalWS * suspensionForce * step;
            btVector3 relpos = wheel.m_raycastInfo.m_contactPointWS - getRigidBody()->getCenterOfMassPosition();

            getRigidBody()->applyImpulse(impulse, relpos);
        }

        updateFriction(step);

        for (i = 0; i < m_wheelInfo.size(); i++)
        {
            CustomWheelInfo& wheel = m_wheelInfo[i];
            
            /*
            // Find velocity of vehicle at wheel contact point. This is no longer
            // needed, since wheel rotation no longer directly depends on ground motion.
            btVector3 relpos = wheel.m_raycastInfo.m_hardPointWS - getRigidBody()->getCenterOfMassPosition();
            btVector3 vel = getRigidBody()->getVelocityInLocalPoint(relpos);
            */
            
            // All we need to do is rotate by angular velocity. Fmod the rotation to keep precision.
            wheel.m_rotation += wheel.m_angularVelocity * step;
            wheel.m_rotation = std::fmod(wheel.m_rotation, 2.0f * M_PI);
            
            /*
            Old calculation for wheel rotation, directly based on ground motion.
            
            if (wheel.m_raycastInfo.m_isInContact)
            {
                const btTransform& chassisWorldTransform = getChassisWorldTransform();
                
                // Get vehicle forward axis direction
                btVector3 fwd(
                    chassisWorldTransform.getBasis()[0][m_indexForwardAxis],
                    chassisWorldTransform.getBasis()[1][m_indexForwardAxis],
                    chassisWorldTransform.getBasis()[2][m_indexForwardAxis]);

                // Normalise forward axis to lie on the surface plane
                btScalar proj = fwd.dot(wheel.m_raycastInfo.m_contactNormalWS);
                fwd -= wheel.m_raycastInfo.m_contactNormalWS * proj;

                // Find velocity in direction of normalised forward axis
                btScalar proj2 = fwd.dot(vel);

                wheel.m_angularVelocity = (proj2 * step) / (wheel.m_wheelsRadius);
                wheel.m_rotation += wheel.m_angularVelocity;
            }
            else
            {
                wheel.m_rotation += wheel.m_angularVelocity;
            }
            */

            // wheel.m_angularVelocity *= btScalar(0.99);  //damping of rotation when not in contact        <><> Don't damp any more
        }
    }

    void CustomRaycastVehicle::setSteeringValue(btScalar steering, int wheel)
    {
        btAssert(wheel >= 0 && wheel < getNumWheels());

        CustomWheelInfo& wheelInfo = getWheelInfo(wheel);
        wheelInfo.m_steering = steering;
    }

    btScalar CustomRaycastVehicle::getSteeringValue(int wheel) const
    {
        return getWheelInfo(wheel).m_steering;
    }
    
    btScalar CustomRaycastVehicle::getWheelAngularVelocity(int wheel) const {
        return getWheelInfo(wheel).m_angularVelocity;
    }
        
    btScalar CustomRaycastVehicle::getWheelBaseFrictionForce(int wheel) const {
        return getWheelInfo(wheel).m_baseFrictionForce;
    }
    
    btScalar CustomRaycastVehicle::getWheelAxleFrictionForce(int wheel) const {
        return getWheelInfo(wheel).m_axleFrictionForce;
    }
    
    void CustomRaycastVehicle::setWheelBaseFrictionForce(btScalar value, int wheel) {
        m_wheelInfo[wheel].m_baseFrictionForce = value;
    }
    
    void CustomRaycastVehicle::setWheelAxleFrictionForce(btScalar value, int wheel) {
        m_wheelInfo[wheel].m_axleFrictionForce = value;
    }

    void CustomRaycastVehicle::applyEngineForce(btScalar force, int wheel)
    {
        btAssert(wheel >= 0 && wheel < getNumWheels());
        CustomWheelInfo& wheelInfo = getWheelInfo(wheel);
        wheelInfo.m_engineForce = force;
    }

    const CustomWheelInfo& CustomRaycastVehicle::getWheelInfo(int index) const
    {
        btAssert((index >= 0) && (index < getNumWheels()));

        return m_wheelInfo[index];
    }

    CustomWheelInfo& CustomRaycastVehicle::getWheelInfo(int index)
    {
        btAssert((index >= 0) && (index < getNumWheels()));

        return m_wheelInfo[index];
    }

    void CustomRaycastVehicle::setBrake(btScalar brake, int wheelIndex)
    {
        btAssert((wheelIndex >= 0) && (wheelIndex < getNumWheels()));
        getWheelInfo(wheelIndex).m_brake = brake;
    }

    void CustomRaycastVehicle::updateSuspension(btScalar deltaTime)
    {
        (void)deltaTime;

        btScalar chassisMass = btScalar(1.) / m_chassisBody->getInvMass();

        for (int w_it = 0; w_it < getNumWheels(); w_it++)
        {
            CustomWheelInfo& wheel_info = m_wheelInfo[w_it];

            if (wheel_info.m_raycastInfo.m_isInContact)
            {
                btScalar force;
                //	Spring
                {
                    btScalar susp_length = wheel_info.getSuspensionRestLength();
                    btScalar current_length = wheel_info.m_raycastInfo.m_suspensionLength;

                    btScalar length_diff = (susp_length - current_length);

                    force = wheel_info.m_suspensionStiffness * length_diff * wheel_info.m_clippedInvContactDotSuspension;
                }

                // Damper
                {
                    btScalar projected_rel_vel = wheel_info.m_suspensionRelativeVelocity;
                    {
                        btScalar susp_damping;
                        if (projected_rel_vel < btScalar(0.0))
                        {
                            susp_damping = wheel_info.m_wheelsDampingCompression;
                        }
                        else
                        {
                            susp_damping = wheel_info.m_wheelsDampingRelaxation;
                        }
                        force -= susp_damping * projected_rel_vel;
                    }
                }

                // RESULT
                wheel_info.m_wheelsSuspensionForce = force * chassisMass;
                if (wheel_info.m_wheelsSuspensionForce < btScalar(0.))
                {
                    wheel_info.m_wheelsSuspensionForce = btScalar(0.);
                }
            }
            else
            {
                wheel_info.m_wheelsSuspensionForce = btScalar(0.0);
            }
        }
    }

    struct btWheelContactPoint
    {
        btRigidBody* m_body0;
        btRigidBody* m_body1;
        btVector3 m_frictionPositionWorld;
        btVector3 m_frictionDirectionWorld;
        btScalar m_jacDiagABInv;
        btScalar m_maxImpulse;

        btWheelContactPoint(btRigidBody* body0, btRigidBody* body1, const btVector3& frictionPosWorld, const btVector3& frictionDirectionWorld, btScalar maxImpulse)
            : m_body0(body0),
              m_body1(body1),
              m_frictionPositionWorld(frictionPosWorld),
              m_frictionDirectionWorld(frictionDirectionWorld),
              m_maxImpulse(maxImpulse)
        {
            btScalar denom0 = body0->computeImpulseDenominator(frictionPosWorld, frictionDirectionWorld);
            btScalar denom1 = body1->computeImpulseDenominator(frictionPosWorld, frictionDirectionWorld);
            btScalar relaxation = 1.f;
            m_jacDiagABInv = relaxation / (denom0 + denom1);
        }
    };

    btScalar calcRollingFriction(btWheelContactPoint& contactPoint, int numWheelsOnGround);
    btScalar calcRollingFriction(btWheelContactPoint& contactPoint, int numWheelsOnGround)
    {
        btScalar j1 = 0.f;

        const btVector3& contactPosWorld = contactPoint.m_frictionPositionWorld;

        btVector3 rel_pos1 = contactPosWorld - contactPoint.m_body0->getCenterOfMassPosition();
        btVector3 rel_pos2 = contactPosWorld - contactPoint.m_body1->getCenterOfMassPosition();

        btScalar maxImpulse = contactPoint.m_maxImpulse;

        btVector3 vel1 = contactPoint.m_body0->getVelocityInLocalPoint(rel_pos1);
        btVector3 vel2 = contactPoint.m_body1->getVelocityInLocalPoint(rel_pos2);
        btVector3 vel = vel1 - vel2;

        btScalar vrel = contactPoint.m_frictionDirectionWorld.dot(vel);

        // calculate j that moves us to zero relative velocity
        j1 = -vrel * contactPoint.m_jacDiagABInv / btScalar(numWheelsOnGround);
        btSetMin(j1, maxImpulse);
        btSetMax(j1, -maxImpulse);

        return j1;
    }

    btScalar sideFrictionStiffness2 = btScalar(1.0);
    void CustomRaycastVehicle::updateFriction(btScalar timeStep)
    {
        //calculate the impulse, so that the wheels don't move sidewards
        int numWheel = getNumWheels();
        if (!numWheel)
            return;
        
        // Mass of whole vehicle.
        btScalar chassisMass = btScalar(1.) / m_chassisBody->getInvMass();


        m_forwardWS.resize(numWheel);
        m_axle.resize(numWheel);
        m_forwardImpulse.resize(numWheel);
        m_sideImpulse.resize(numWheel);

        int numWheelsOnGround = 0;

        //collapse all those loops into one!
        for (int i = 0; i < getNumWheels(); i++)
        {
            CustomWheelInfo& wheelInfo = m_wheelInfo[i];
            class btRigidBody* groundObject = (class btRigidBody*)wheelInfo.m_raycastInfo.m_groundObject;
            if (groundObject)
                numWheelsOnGround++;
            m_sideImpulse[i] = btScalar(0.);
            m_forwardImpulse[i] = btScalar(0.);
        }

        {
            for (int i = 0; i < getNumWheels(); i++)
            {
                CustomWheelInfo& wheelInfo = m_wheelInfo[i];

                class btRigidBody* groundObject = (class btRigidBody*)wheelInfo.m_raycastInfo.m_groundObject;

                if (groundObject)
                {
                    const btTransform& wheelTrans = getWheelTransformWS(i);

                    btMatrix3x3 wheelBasis0 = wheelTrans.getBasis();
                    m_axle[i] = -btVector3(
                        wheelBasis0[0][m_indexRightAxis],
                        wheelBasis0[1][m_indexRightAxis],
                        wheelBasis0[2][m_indexRightAxis]);

                    const btVector3& surfNormalWS = wheelInfo.m_raycastInfo.m_contactNormalWS;
                    btScalar proj = m_axle[i].dot(surfNormalWS);
                    m_axle[i] -= surfNormalWS * proj;
                    m_axle[i] = m_axle[i].normalize();

                    m_forwardWS[i] = surfNormalWS.cross(m_axle[i]);
                    m_forwardWS[i].normalize();

                    resolveSingleBilateral(*m_chassisBody, wheelInfo.m_raycastInfo.m_contactPointWS,
                                           *groundObject, wheelInfo.m_raycastInfo.m_contactPointWS,
                                           btScalar(0.), m_axle[i], m_sideImpulse[i], timeStep);

                    m_sideImpulse[i] *= sideFrictionStiffness2;
                }
            }
        }

        btScalar sideFactor = btScalar(1.);
        btScalar fwdFactor = 0.5;

        {
            for (int wheel = 0; wheel < getNumWheels(); wheel++)
            {
                CustomWheelInfo& wheelInfo = m_wheelInfo[wheel];
                class btRigidBody* groundObject = (class btRigidBody*)wheelInfo.m_raycastInfo.m_groundObject;

                ////////////////////////////////////////////////////////////////
                
                // Maximum impulse, when in contact with ground.
                btScalar maximp = wheelInfo.m_wheelsSuspensionForce * timeStep * wheelInfo.m_frictionSlip;
                
                // Find velocity in direction of normalised forward axis, if contacting ground.
                btScalar fwdVel;
                if (groundObject != NULL) {
                    // Get (global) car velocity at local point of contact point of wheel.
                    btVector3 relpos = wheelInfo.m_raycastInfo.m_hardPointWS - getRigidBody()->getCenterOfMassPosition();
                    btVector3 vel = getRigidBody()->getVelocityInLocalPoint(relpos);
                    
                    // Get vehicle forward axis direction
                    const btTransform& chassisWorldTransform = getChassisWorldTransform();
                    btVector3 fwd(
                        chassisWorldTransform.getBasis()[0][m_indexForwardAxis],
                        chassisWorldTransform.getBasis()[1][m_indexForwardAxis],
                        chassisWorldTransform.getBasis()[2][m_indexForwardAxis]);

                    // Normalise forward axis to lie on the surface plane, then find velocity in this direction.
                    btScalar proj = fwd.dot(wheelInfo.m_raycastInfo.m_contactNormalWS);
                    fwd -= wheelInfo.m_raycastInfo.m_contactNormalWS * proj;
                    fwdVel = fwd.dot(vel);
                }
                
                // Rolling friction is impulse to apply to wheel rotation.
                // Braking impulse should be applied directly.
                btScalar rollingFriction = 0.0f,
                         brakingImpulse  = 0.0f;
                
                // For braking, directly use the brake force when not in contact with ground.
                // Otherwise, set braking impulse to the force which we can achieve.
                if (wheelInfo.m_engineForce == 0.0f) {
                    if (groundObject == NULL) {
                        // Negate wheel rotation.
                        rollingFriction = std::abs(wheelInfo.m_brake * timeStep);
                        rollingFriction = std::min(rollingFriction, std::abs(wheelInfo.m_angularVelocity * wheelInfo.m_inertia));
                        makeOppositeSign(wheelInfo.m_angularVelocity, rollingFriction);
                    
                    } else {
                        // Set braking impulse to maximum achievable (not more than car's momentum) (in direction of velocity).
                        brakingImpulse = std::abs(wheelInfo.m_brake * timeStep);
                        brakingImpulse = std::min(brakingImpulse, std::abs((fwdVel * chassisMass) / numWheelsOnGround));
                        makeOppositeSign(fwdVel, brakingImpulse);
                    }
                
                // When accelerating, just use engine force as rolling friction.
                } else {
                    rollingFriction = wheelInfo.m_engineForce * timeStep;
                }
                
                // Subtract axle friction, add engine force (rollingFriction), then subtract base friction.
                signlessZeroBarrierSubtract(wheelInfo.m_angularVelocity, wheelInfo.m_axleFrictionForce * wheelInfo.m_angularVelocity * timeStep / wheelInfo.m_inertia);
                wheelInfo.m_angularVelocity += (rollingFriction / wheelInfo.m_inertia);
                signlessZeroBarrierSubtract(wheelInfo.m_angularVelocity, wheelInfo.m_baseFrictionForce * timeStep / wheelInfo.m_inertia);
                
                m_forwardImpulse[wheel] = 0.0f;
                wheelInfo.m_skidInfo    = 1.0f;

                if (groundObject != NULL) {
                    
                    // Find impulse from tyre slip. Dividing slip velocity by radius, then multiplying by inertia
                    // works out difference in angular momentum. Divide this *again* by the wheel's radius: We're
                    // going from torque to linear force, and larger wheels provide less linear force due to their size.
                    btScalar tyreSlipVel     = wheelInfo.m_angularVelocity * wheelInfo.m_wheelsRadius - fwdVel,
                             tyreSlipImpulse = (tyreSlipVel * wheelInfo.m_inertia) / (wheelInfo.m_wheelsRadius * wheelInfo.m_wheelsRadius);
                    
                    // Use tyre slip impulse as forward impulse unless the braking impulse exists.
                    if (brakingImpulse == 0.0f) {
                        m_forwardImpulse[wheel] = tyreSlipImpulse;
                    } else {
                        m_forwardImpulse[wheel] = brakingImpulse;
                    }
                    
                    // Figure out total impulse and maximum impulses squared.
                    btScalar maximpSide = maximp;
                    btScalar maximpSquared = maximp * maximpSide;
                    btScalar x = (m_forwardImpulse[wheel]) * fwdFactor;
                    btScalar y = (m_sideImpulse[wheel]) * sideFactor;
                    btScalar impulseSquared = (x * x + y * y);

                    // If impulse squared is more than maximum, diminish wheel impulses.
                    if (impulseSquared > maximpSquared) {
                        btScalar factor = maximp / btSqrt(impulseSquared);
                        wheelInfo.m_skidInfo *= factor;
                        // Multiply impulses by skid factor: diminish impulses as wheels skid more.
                        if (m_sideImpulse[wheel] != 0.0f) {
                            m_forwardImpulse[wheel] *= wheelInfo.m_skidInfo;
                            m_sideImpulse[wheel]    *= wheelInfo.m_skidInfo;
                        }
                    }
                    
                    // If there is no braking impulse, subtract the USED tyre slip impulse from the
                    // angular velocity: We are robbing momentum from the wheel and giving it to the car.
                    // Multiply by the wheel's radius here, since we are converting linear force to torque.
                    if (brakingImpulse == 0.0f) {
                        wheelInfo.m_angularVelocity -= (tyreSlipImpulse * wheelInfo.m_wheelsRadius * wheelInfo.m_skidInfo / wheelInfo.m_inertia);
                    
                    // Otherwise, subtract the total tyre slip impulse, THEN subtract the UNUSED braking impulse from
                    // the angular velocity: Any unused braking impulse goes to lock up the wheel, (if subtracting
                    // swapped the sign, set to zero since it ate all momentum).
                    } else {
                        zeroBarrierSubtract(wheelInfo.m_angularVelocity, tyreSlipImpulse * wheelInfo.m_wheelsRadius / wheelInfo.m_inertia);
                        btScalar unusedBrake = brakingImpulse * (1.0f - wheelInfo.m_skidInfo) / wheelInfo.m_inertia;
                        signlessZeroBarrierSubtract(wheelInfo.m_angularVelocity, unusedBrake);
                    }
                }
            }
        }

        // apply the impulses
        {
            for (int wheel = 0; wheel < getNumWheels(); wheel++)
            {
                CustomWheelInfo& wheelInfo = m_wheelInfo[wheel];

                btVector3 rel_pos = wheelInfo.m_raycastInfo.m_contactPointWS -
                                    m_chassisBody->getCenterOfMassPosition();

                if (m_forwardImpulse[wheel] != btScalar(0.))
                {
                    m_chassisBody->applyImpulse(m_forwardWS[wheel] * (m_forwardImpulse[wheel]), rel_pos);
                }
                if (m_sideImpulse[wheel] != btScalar(0.))
                {
                    class btRigidBody* groundObject = (class btRigidBody*)m_wheelInfo[wheel].m_raycastInfo.m_groundObject;

                    btVector3 rel_pos2 = wheelInfo.m_raycastInfo.m_contactPointWS -
                                         groundObject->getCenterOfMassPosition();

                    btVector3 sideImp = m_axle[wheel] * m_sideImpulse[wheel];

    #if defined ROLLING_INFLUENCE_FIX  // fix. It only worked if car's up was along Y - VT.
                    btVector3 vChassisWorldUp = getRigidBody()->getCenterOfMassTransform().getBasis().getColumn(m_indexUpAxis);
                    rel_pos -= vChassisWorldUp * (vChassisWorldUp.dot(rel_pos) * (1.f - wheelInfo.m_rollInfluence));
    #else
                    rel_pos[m_indexUpAxis] *= wheelInfo.m_rollInfluence;
    #endif
                    m_chassisBody->applyImpulse(sideImp, rel_pos);

                    //apply friction impulse on the ground
                    groundObject->applyImpulse(-sideImp, rel_pos2);
                }
            }
        }
    }

    void CustomRaycastVehicle::debugDraw(btIDebugDraw* debugDrawer)
    {
        for (int v = 0; v < this->getNumWheels(); v++)
        {
            btVector3 wheelColor(0, 1, 1);
            if (getWheelInfo(v).m_raycastInfo.m_isInContact)
            {
                wheelColor.setValue(0, 0, 1);
            }
            else
            {
                wheelColor.setValue(1, 0, 1);
            }

            btVector3 wheelPosWS = getWheelInfo(v).m_worldTransform.getOrigin();

            btVector3 axle = btVector3(
                getWheelInfo(v).m_worldTransform.getBasis()[0][getRightAxis()],
                getWheelInfo(v).m_worldTransform.getBasis()[1][getRightAxis()],
                getWheelInfo(v).m_worldTransform.getBasis()[2][getRightAxis()]);

            //debug wheels (cylinders)
            debugDrawer->drawLine(wheelPosWS, wheelPosWS + axle, wheelColor);
            debugDrawer->drawLine(wheelPosWS, getWheelInfo(v).m_raycastInfo.m_contactPointWS, wheelColor);
        }
    }
}
