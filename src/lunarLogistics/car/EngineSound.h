/*
 * EngineSound.h
 *
 *  Created on: 20 Sep 2020
 *      Author: wilson
 */

#ifndef LUNARLOGISTICS_CAR_ENGINESOUND_H_
#define LUNARLOGISTICS_CAR_ENGINESOUND_H_

#include <rascUI/util/EmplaceVector.h>
#include <AL/al.h>
#include <cstdint>
#include <string>

namespace threading {
    class ThreadQueue;
}

namespace tinyxml2 {
    class XMLElement;
}

namespace lunarlogistics {

    /**
     *
     */
    class EngineSound {
    private:
    
        // Internal class for representing sounds.
        class Sound {
        private:
            ALuint buffer, source;
            float engineSpeed;
            int16_t * data;
            bool playing;
            
            Sound(const Sound & other);
            Sound & operator = (const Sound & other);
            
        public:
            Sound(const char * filename, float engineSpeed, size_t soundLength);
            ~Sound(void);

            // Loads OpenAL resources. Returns true if successful.
            bool loadResources(size_t soundLength, size_t sampleRate);
            
            // Plays the sound, simulating the specified engine speed, using the specified gain.
            void playEngineSpeed(float realEngineSpeed, float gain);
            
            // Makes sure the sound is NOT playing.
            void pause(void);
            
            // Accesses our engine speed.
            inline float getEngineSpeed(void) const {
                return engineSpeed;
            }
        };
    
        float volumeMultiplier;
        threading::ThreadQueue * threadQueue;
        void * token;
        bool loaded;
        uint64_t loadJob;
        
        unsigned int sampleRate;
        size_t soundLength;
        float minSpeed, maxSpeed;
        rascUI::EmplaceVector<Sound> sounds;

    public:
        EngineSound(threading::ThreadQueue * threadQueue, const std::string & soundDirectory, float volumeMultiplier = 1.0f);
        ~EngineSound(void);
        
    private:
        // Run when loading XML failed.
        void setupFailed(void);
    
        // Load job, run on separate thread.
        void load(const std::string & soundDirectory);
        
        // Sets up OpenAL resources, run on main thread after load job finishes.
        void onLoaded(void);
        
    public:
        /**
         * This should be called each frame - it updates the sound
         * according to the current engine speed. This does nothing
         * until after we have finished loading.
         */
        void updateEngineSpeed(float engineSpeed);
    };
}

#endif
