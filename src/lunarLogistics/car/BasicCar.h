/*
 * BasicCar.h
 *
 *  Created on: 25 Jul 2020
 *      Author: wilson
 */

#ifndef LUNARLOGISTICS_CAR_BASICCAR_H_
#define LUNARLOGISTICS_CAR_BASICCAR_H_

#include <bullet3/BulletDynamics/Dynamics/btActionInterface.h>
#include <bullet3/BulletDynamics/Vehicle/btRaycastVehicle.h>
#include <awe/instance/Instance.h>
#include <awe/util/types/Types.h>
#include <string>

#include "TorqueCurve.h"
#include "Gearbox.h"
#include "Engine.h"

namespace awe {
    class AssetManager;
    class PhysicsWorld;
    class WorldManager;
    class HeightMap;
}

namespace lunarlogistics {
    class CustomRaycastVehicle;

    /**
     *
     */
    class BasicCar : public btActionInterface {
    private:
        std::string name;
        awe::GLvec3 initialPos;
        bool loaded;
    
        awe::AssetManager * assetManager;
        awe::PhysicsWorld * physicsWorld;
        awe::WorldManager * worldManager;
        
        awe::Instance wheelFL, wheelFR, wheelBL, wheelBR;
        awe::Instance * car;
        
        btDefaultVehicleRaycaster raycaster;
        CustomRaycastVehicle * raycastVehicle; // Initialised during initCar.
        
        bool accelerating, braking, steeringLeft, steeringRight;
        
        GLfloat steeringAngle;
        
        TorqueCurve torqueCurve;
        Gearbox gearbox;
        Engine engine;
        
        BasicCar(const BasicCar & other);
        BasicCar & operator = (const BasicCar & other);
        
    public:
        BasicCar(awe::AssetManager * assetManager, awe::PhysicsWorld * physicsWorld, awe::WorldManager * worldManager, const std::string & name);
        virtual ~BasicCar(void);
        
    private:
        void initCar(void);
        
        void getWheelTransform(awe::GLmat4 & transform, int wheelIndex);
        
    public:
        // Implementation of bullet action interface. Note that updateAction is not actually
        // called until we are loaded.
        void updateAction(btCollisionWorld * collisionWorld, btScalar deltaTimeStep) override;
        void debugDraw(btIDebugDraw * debugDrawer) override;
    
        void step(GLfloat elapsed /*, awe::HeightMap * heightMap*/);
        
        /**
         * Returns our associated car instance, Or NULL if it has not been
         * assigned yet.
         */
        inline awe::Instance * getCarInstance(void) const {
            return car;
        }
        
        void onKeyPress(int key, bool special);
        void onKeyRelease(int key, bool special);
        
        /**
         * Accesses the car's speed (units per second), or zero if we have not
         * loaded the car yet.
         */
        GLfloat getSpeed(void) const;
        
        /**
         * Resets the position of the car.
         */
        void resetPosition(void);
        
        /**
         * Resets the rotation of the car, and moves it upwards by a bit.
         */
        void flip(void);
        
        /**
         * Allows access to our internal gearbox - helpful for the UI system.
         */
        inline const Gearbox & getGearbox(void) const {
            return gearbox;
        }
        
        /**
         * Allows access to our internal engine - helpful for the UI system.
         */
        inline const Engine & getEngine(void) const {
            return engine;
        }
        
        /**
         * Gets the rotational speed of the engine, converted to revolutions
         * per minute, for displaying in the UI system.
         */
        GLfloat getEngineRPM(void) const;
        
        /**
         * Returns the maximum engine rotational speed (RPM). Note that this is
         * an estimate: real engine RPM may be higher than this, but any display
         * in the UI system should consider *this* to be the maximum RPM.
         */
        GLfloat getMaxEngineRPM(void) const;
        
        /**
         * Gets the car's current speed, from the average of speed of rotation
         * of both of the car's drive wheels (in units per second).
         * This will return roughly the same as getSpeed(), when the wheels are
         * not slipping. This is the speed measurement that a car dashboard
         * would more realistically show.
         */
        GLfloat getAverageDriveWheelSpeed(void) const;
    };
}

#endif
