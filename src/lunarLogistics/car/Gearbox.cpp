/*
 * Gearbox.cpp
 *
 *  Created on: 14 Sep 2020
 *      Author: wilson
 */

#include "Gearbox.h"

#include <algorithm>

namespace lunarlogistics {

    Gearbox::Gearbox(std::initializer_list<std::pair<std::string, float>> gears) :
        gearCount(gears.size()),
        currentGear(0),
        ratios(),
        names() {
        ratios.reserve(gearCount);
        names.reserve(gearCount);
        for (const std::pair<std::string, float> & gear : gears) {
            names.push_back(gear.first);
            ratios.push_back(gear.second);
        }
    }
    
    void Gearbox::changeGearBy(int amount) {
        // Shifting down
        if (amount < 0) {
            currentGear -= std::min(currentGear, (unsigned int)(-amount));
            
        // Shifting up
        } else {
            currentGear = std::min(currentGear + (unsigned int)amount, gearCount - 1);
        }
    }
    
    bool Gearbox::isEngaged(void) const {
        return ratios[currentGear] != 0.0f;
    }
    
    float Gearbox::engineToWheelSpeed(float engineSpeed) const {
        return engineSpeed * ratios[currentGear];
    }
    
    float Gearbox::engineToWheelTorque(float engineTorque) const {
        float ratio = ratios[currentGear];
        // Always return zero for neutral.
        if (ratio == 0.0f) {
            return 0.0f;
        }
        return engineTorque / ratio;
    }
}
