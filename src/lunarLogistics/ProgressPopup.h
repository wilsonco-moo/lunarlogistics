/*
 * ProgressPopup.h
 *
 *  Created on: 29 Aug 2020
 *      Author: wilson
 */

#ifndef LUNARLOGISTICS_PROGRESSPOPUP_H_
#define LUNARLOGISTICS_PROGRESSPOPUP_H_

#include <rascUI/components/floating/FloatingPanel.h>
#include <rascUI/components/generic/ProgressBar.h>
#include <rascUI/components/generic/FrontPanel.h>
#include <rascUI/components/generic/Button.h>
#include <rascUI/components/generic/Label.h>
#include <awe/util/ProgressControl.h>

namespace rascUI {
    class Theme;
}

namespace lunarlogistics {

    /**
     *
     */
    class ProgressPopup : public rascUI::FloatingPanel {
    private:
        awe::ProgressControl progressControl;
        rascUI::ProgressBar progressBar;
        rascUI::FrontPanel bottomPanel;
        rascUI::Label bottomLabel;
        rascUI::Button startButton;
    
    public:
        ProgressPopup(rascUI::Theme * theme);
        virtual ~ProgressPopup(void);
        
        void updateRemaining(unsigned int remaining);
    };
}

#endif
